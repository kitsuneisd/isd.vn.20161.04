1. Phân công phân tích thiết kế UC.
	Trần Thị Như Hoa:
		+) search book
		+) borrow book
	Nguyễn Phúc Long:
		+) accept return book
        	+) add a new copy
	Lê Đình Phúc:
		+) User login.
		+) Browse borrow books.
2: Phân công review.
	Trần Thị Như Hoa 	-> Lê Đình Phúc
	Lê Đình Phúc 		-> Nguyễn Phúc Long
	Nguyễn Phúc Long 	-> Trần Thị Như Hoa
3. Phân công lập trình.
	Trần Thị Như Hoa:
		+) Lập trình các chức năng: search book, borrow book.
	Nguyễn Phúc Long:
		+) Lập trình các chức năng: accept return book, add a new copy.
	Lê Đình Phúc:
		+) Tạo dự án và các lớp cơ bản.
		+) Xây dựng database.
		+) Lập trình các chức năg: User login, Browse borrow books, menu user.
4. Phân công kiểm thử.
	Trần Thị Như Hoa:
		+) Kiểm thử các chức năng: search book, borrow book.
	Nguyễn Phúc Long:
		+) Kiểm thử các chức năng: accept return book, add a new copy.
	Lê Đình Phúc:
		+) Kiểm thử chức năg: User login, Browse borrow books, menu user.