package function.boundary.librarian;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.GridBagLayout;
import javax.swing.JButton;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class ManageCopyUI extends JFrame {

	private JPanel contentPane;

//	/**
//	 * Launch the application.
//	 */
//	public static void main(String[] args) {
//		EventQueue.invokeLater(new Runnable() {
//			public void run() {
//				try {
//					ManageCopyUI frame = new ManageCopyUI();
//					frame.setVisible(true);
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//			}
//		});
//	}

	/**
	 * Create the frame.
	 */
	public ManageCopyUI() {
		setTitle("Manage Copy");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 331, 168);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[]{0, 0, 0, 0};
		gbl_contentPane.rowHeights = new int[]{0, 0, 0, 0};
		gbl_contentPane.columnWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_contentPane.rowWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
		contentPane.setLayout(gbl_contentPane);
		
		JButton btnAddNewCopy = new JButton("Add new copy");
		GridBagConstraints gbc_btnAddNewCopy = new GridBagConstraints();
		gbc_btnAddNewCopy.insets = new Insets(0, 0, 5, 5);
		gbc_btnAddNewCopy.gridx = 0;
		gbc_btnAddNewCopy.gridy = 0;
		contentPane.add(btnAddNewCopy, gbc_btnAddNewCopy);
		
		JButton btnEditACopy = new JButton("Edit a copy");
		btnEditACopy.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		GridBagConstraints gbc_btnEditACopy = new GridBagConstraints();
		gbc_btnEditACopy.insets = new Insets(0, 0, 5, 0);
		gbc_btnEditACopy.gridx = 2;
		gbc_btnEditACopy.gridy = 0;
		contentPane.add(btnEditACopy, gbc_btnEditACopy);
		
		JButton btnDeleteACopy = new JButton("Delete a copy");
		GridBagConstraints gbc_btnDeleteACopy = new GridBagConstraints();
		gbc_btnDeleteACopy.insets = new Insets(0, 0, 0, 5);
		gbc_btnDeleteACopy.gridx = 0;
		gbc_btnDeleteACopy.gridy = 2;
		contentPane.add(btnDeleteACopy, gbc_btnDeleteACopy);
		
		JButton btnSearchACopy = new JButton("Search a copy");
		GridBagConstraints gbc_btnSearchACopy = new GridBagConstraints();
		gbc_btnSearchACopy.gridx = 2;
		gbc_btnSearchACopy.gridy = 2;
		contentPane.add(btnSearchACopy, gbc_btnSearchACopy);
		
		btnAddNewCopy.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				new AddNewCopyUI().setVisible(true);
			}
		});
	}
}
