package function.boundary.librarian;

import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import common.Constants;
import function.controller.librarian.AcceptReturnBookController;
import function.controller.librarian.BookCopyInfo;
import function.controller.librarian.BorrowingInfo;

public class AcceptReturnBookUI extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField txtBorrowerCardNumber;
	private JTextField txtCopyNumber;
	private JButton btnAdd;
	private JSeparator separator;
	private JLabel lblBorrowerName;
	private JTextField txtBorrowerName;
	private JPanel panel;
	private JButton btnFinish;
	private JButton btnCancel;
	private JButton btnSearch;

	private Object[] colName = { "Select", "Copy number", "Book name", "Borrowed date", "Intended Return Date",
			"Note" };
	private JScrollPane scroll;
	private JTable jtable_ListCopy;
	private DefaultTableModel dataModel;
	private JSeparator separator_1;
	private BorrowingInfo bi;

//	/**
//	 * Launch the application.
//	 */
//	public static void main(String[] args) {
//		EventQueue.invokeLater(new Runnable() {
//			public void run() {
//				try {
//					AcceptReturnBookUI frame = new AcceptReturnBookUI();
//					frame.setVisible(true);
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//			}
//		});
//	}

	/**
	 * Create the frame.
	 */
	public AcceptReturnBookUI() {
		setTitle("Accept return book");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 721, 444);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[] { 0, 0, 0, 0 };
		gbl_contentPane.rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
		gbl_contentPane.columnWeights = new double[] { 0.0, 1.0, 1.0, Double.MIN_VALUE };
		gbl_contentPane.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
				1.0, Double.MIN_VALUE };
		contentPane.setLayout(gbl_contentPane);

		JLabel lblBorrowerCardNumber = new JLabel("Borrower card number");
		GridBagConstraints gbc_lblBorrowerCardNumber = new GridBagConstraints();
		gbc_lblBorrowerCardNumber.insets = new Insets(0, 0, 5, 5);
		gbc_lblBorrowerCardNumber.anchor = GridBagConstraints.WEST;
		gbc_lblBorrowerCardNumber.gridx = 0;
		gbc_lblBorrowerCardNumber.gridy = 0;
		contentPane.add(lblBorrowerCardNumber, gbc_lblBorrowerCardNumber);

		txtBorrowerCardNumber = new JTextField();
		GridBagConstraints gbc_txtBorrowerCardNumber = new GridBagConstraints();
		gbc_txtBorrowerCardNumber.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtBorrowerCardNumber.insets = new Insets(0, 0, 5, 5);
		gbc_txtBorrowerCardNumber.gridx = 1;
		gbc_txtBorrowerCardNumber.gridy = 0;
		contentPane.add(txtBorrowerCardNumber, gbc_txtBorrowerCardNumber);
		txtBorrowerCardNumber.setColumns(12);

		btnSearch = new JButton("Search");
		btnSearch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		GridBagConstraints gbc_Search = new GridBagConstraints();
		gbc_Search.anchor = GridBagConstraints.WEST;
		gbc_Search.insets = new Insets(0, 0, 5, 0);
		gbc_Search.gridx = 2;
		gbc_Search.gridy = 0;
		contentPane.add(btnSearch, gbc_Search);

		JLabel lblCopyNumber = new JLabel("Copy number");
		GridBagConstraints gbc_lblCopyNumber = new GridBagConstraints();
		gbc_lblCopyNumber.anchor = GridBagConstraints.WEST;
		gbc_lblCopyNumber.insets = new Insets(0, 0, 5, 5);
		gbc_lblCopyNumber.gridx = 0;
		gbc_lblCopyNumber.gridy = 1;
		contentPane.add(lblCopyNumber, gbc_lblCopyNumber);

		txtCopyNumber = new JTextField();
		GridBagConstraints gbc_txtCopyNumber = new GridBagConstraints();
		gbc_txtCopyNumber.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtCopyNumber.weightx = 1.0;
		gbc_txtCopyNumber.insets = new Insets(0, 0, 5, 5);
		gbc_txtCopyNumber.gridx = 1;
		gbc_txtCopyNumber.gridy = 1;
		contentPane.add(txtCopyNumber, gbc_txtCopyNumber);
		txtCopyNumber.setColumns(12);

		btnAdd = new JButton("Add");
		GridBagConstraints gbc_btnAdd = new GridBagConstraints();
		gbc_btnAdd.insets = new Insets(0, 0, 5, 0);
		gbc_btnAdd.anchor = GridBagConstraints.WEST;
		gbc_btnAdd.gridx = 2;
		gbc_btnAdd.gridy = 1;
		contentPane.add(btnAdd, gbc_btnAdd);

		separator = new JSeparator();
		GridBagConstraints gbc_separator = new GridBagConstraints();
		gbc_separator.insets = new Insets(0, 0, 5, 5);
		gbc_separator.gridx = 0;
		gbc_separator.gridy = 2;
		contentPane.add(separator, gbc_separator);
		
		separator_1 = new JSeparator();
		GridBagConstraints gbc_separator_1 = new GridBagConstraints();
		gbc_separator_1.gridwidth = 3;
		gbc_separator_1.insets = new Insets(0, 0, 5, 5);
		gbc_separator_1.gridx = 0;
		gbc_separator_1.gridy = 3;
		contentPane.add(separator_1, gbc_separator_1);

		lblBorrowerName = new JLabel("Borrower name");
		GridBagConstraints gbc_lblBorrowerName = new GridBagConstraints();
		gbc_lblBorrowerName.anchor = GridBagConstraints.WEST;
		gbc_lblBorrowerName.insets = new Insets(0, 0, 5, 5);
		gbc_lblBorrowerName.gridx = 0;
		gbc_lblBorrowerName.gridy = 4;
		contentPane.add(lblBorrowerName, gbc_lblBorrowerName);

		txtBorrowerName = new JTextField();
		txtBorrowerName.setEditable(false);
		GridBagConstraints gbc_txtBorrowerName = new GridBagConstraints();
		gbc_txtBorrowerName.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtBorrowerName.insets = new Insets(0, 0, 5, 5);
		gbc_txtBorrowerName.gridx = 1;
		gbc_txtBorrowerName.gridy = 4;
		contentPane.add(txtBorrowerName, gbc_txtBorrowerName);
		txtBorrowerName.setColumns(16);

		scroll = new JScrollPane();
		GridBagConstraints gbc_scroll = new GridBagConstraints();
		gbc_scroll.gridwidth = 3;
		gbc_scroll.gridheight = 6;
		gbc_scroll.insets = new Insets(0, 0, 5, 0);
		gbc_scroll.fill = GridBagConstraints.BOTH;
		gbc_scroll.gridx = 0;
		gbc_scroll.gridy = 5;
		contentPane.add(scroll, gbc_scroll);

		// sPanel = new JPanel();
		// sPanel.setLayout(new BorderLayout(0, 0));

		jtable_ListCopy = new JTable() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public Class<?> getColumnClass(int column) {
				switch (column) {
				case 0:
					return Boolean.class;
				default:
					return String.class;
				}
			}
		};

		jtableInit();
		// sPanel.add(jtable_ListCopy);
		scroll.setViewportView(jtable_ListCopy);

		panel = new JPanel();
		GridBagConstraints gbc_panel = new GridBagConstraints();
		gbc_panel.gridwidth = 3;
		gbc_panel.insets = new Insets(0, 0, 5, 0);
		gbc_panel.fill = GridBagConstraints.BOTH;
		gbc_panel.gridx = 0;
		gbc_panel.gridy = 12;
		contentPane.add(panel, gbc_panel);
		panel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

		btnFinish = new JButton("Finish");
		panel.add(btnFinish);

		btnCancel = new JButton("Remove All");
		panel.add(btnCancel);
		
		initActionListener();
	}

	private void initActionListener() {
		btnCancel.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				removeAllRow();
				bi = null;
				txtBorrowerName.setText("");
			}
		});
		
		btnSearch.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				removeAllRow();
				searchLentBooks();
			}
		});
		
		btnAdd.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				searchLentBook();
			}
		});
		
		btnFinish.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				BorrowingInfo tmp = updateListBookCopy();
				AcceptReturnBookController.updateBorrowingInfo(tmp);
				removeAllRow();
				bi = null;
				JOptionPane.showMessageDialog(null, "Hoàn thành");
			}
		});
	}
	
	private void searchLentBooks() {
		AcceptReturnBookController.searchLentBooks(txtBorrowerCardNumber.getText(), this);
	}
	
	private void searchLentBook() {
		AcceptReturnBookController.searchLentBook(txtCopyNumber.getText(), this);
	}
	
	private void removeAllRow() {
		int i = dataModel.getRowCount();
		while (i > 0) {
			dataModel.removeRow(i-1);
			i--;
		}
	}
	
	public void showResult(BorrowingInfo bi) {
		this.bi = bi;
		txtBorrowerName.setText(bi.getBorrowedName());
		for (BookCopyInfo bci : bi.getListBookCopy()) {
			if (bci.getState() == Constants.LENT) {
				Object[] data = { false, bci.getBookID() + bci.getCopySenquent(), bci.getBookName(), bci.getBorrowedDate(),
						bci.getIntendedReturnDate(), bci.getNote() };
				dataModel.addRow(data);
			}
		}
		jtable_ListCopy.setPreferredScrollableViewportSize(jtable_ListCopy.getPreferredSize());
	}
	
	public void showResult(BookCopyInfo bci, String userName) {
		if (bi != null) {
			if (checkBookCopyExist(bci)) return;
		} else {
			bi = new BorrowingInfo();
		}
		txtBorrowerName.setText(userName);
		bi.getListBookCopy().add(bci);
		if (bci.getState() == Constants.LENT) {
			Object[] data = { true, bci.getBookID() + bci.getCopySenquent(), bci.getBookName(), bci.getBorrowedDate(),
					bci.getIntendedReturnDate(), bci.getNote() };
			dataModel.addRow(data);
		}
		jtable_ListCopy.setPreferredScrollableViewportSize(jtable_ListCopy.getPreferredSize());
	}
	
	private boolean checkBookCopyExist(BookCopyInfo bci) {
		if (bi == null) return false;
		
		for (BookCopyInfo tmp : bi.getListBookCopy()) {
			if (tmp.getBookID().equals(bci.getBookID())
					&& tmp.getCopySenquent() == bci.getCopySenquent()) {
				return true;
			}
		}
		return false;
	}
	
	private BorrowingInfo updateListBookCopy() {
		BorrowingInfo tmp = new BorrowingInfo();
		int numrow = dataModel.getRowCount();
		
		for (int i = 0; i < numrow; i++) {
			Object value = dataModel.getValueAt(i, 0);
			if (((Boolean)value).booleanValue()) {
				Object note = dataModel.getValueAt(i, 5);
				String snote;
				if (note != null) {
					snote = note.toString();
				} else {
					snote = "";
				}
				System.out.println(snote);
				bi.getListBookCopy().get(i).setNote(snote);
				tmp.getListBookCopy().add(bi.getListBookCopy().get(i));
			}
		}
		return tmp;
	}

	private void jtableInit() {
		dataModel = new DefaultTableModel();
		dataModel.setColumnIdentifiers(colName);
		jtable_ListCopy.setModel(dataModel);
	}

}
