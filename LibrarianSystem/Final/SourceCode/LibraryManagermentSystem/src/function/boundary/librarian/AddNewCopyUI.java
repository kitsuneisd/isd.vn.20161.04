package function.boundary.librarian;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import function.controller.librarian.ManageCopyController;

public class AddNewCopyUI extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField txtBookNumber;
	private JTextField txtPriceOfCopy;
	private JComboBox<String> cbbTypeOfCopy;
	private JButton btnAdd;
	
	/**
	 * Launch the application.
	 */

	/**
	 * Create the frame.
	 */
	public AddNewCopyUI() {
		setTitle("Add new copy");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 362, 238);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[]{0, 0, 0, 0};
		gbl_contentPane.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0};
		gbl_contentPane.columnWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_contentPane.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		contentPane.setLayout(gbl_contentPane);
		
		JLabel lblBookNumber = new JLabel("Book number");
		GridBagConstraints gbc_lblBookNumber = new GridBagConstraints();
		gbc_lblBookNumber.anchor = GridBagConstraints.WEST;
		gbc_lblBookNumber.insets = new Insets(0, 0, 5, 5);
		gbc_lblBookNumber.gridx = 0;
		gbc_lblBookNumber.gridy = 1;
		contentPane.add(lblBookNumber, gbc_lblBookNumber);
		
		txtBookNumber = new JTextField();
		GridBagConstraints gbc_txtBookNumber = new GridBagConstraints();
		gbc_txtBookNumber.insets = new Insets(0, 0, 5, 0);
		gbc_txtBookNumber.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtBookNumber.gridx = 2;
		gbc_txtBookNumber.gridy = 1;
		contentPane.add(txtBookNumber, gbc_txtBookNumber);
		txtBookNumber.setColumns(15);
		
		JLabel lblTypeOfCopy = new JLabel("Type of copy");
		GridBagConstraints gbc_lblTypeOfCopy = new GridBagConstraints();
		gbc_lblTypeOfCopy.anchor = GridBagConstraints.WEST;
		gbc_lblTypeOfCopy.insets = new Insets(0, 0, 5, 5);
		gbc_lblTypeOfCopy.gridx = 0;
		gbc_lblTypeOfCopy.gridy = 2;
		contentPane.add(lblTypeOfCopy, gbc_lblTypeOfCopy);
		
		cbbTypeOfCopy = new JComboBox<String>();
		GridBagConstraints gbc_cbbTypeOfCopy = new GridBagConstraints();
		gbc_cbbTypeOfCopy.insets = new Insets(0, 0, 5, 0);
		gbc_cbbTypeOfCopy.fill = GridBagConstraints.HORIZONTAL;
		gbc_cbbTypeOfCopy.gridx = 2;
		gbc_cbbTypeOfCopy.gridy = 2;
		contentPane.add(cbbTypeOfCopy, gbc_cbbTypeOfCopy);
		
		cbbTypeOfCopy.addItem("Reference");
		cbbTypeOfCopy.addItem("Borrowable");
		
		JLabel lblPriceOfCopy = new JLabel("Price of copy");
		GridBagConstraints gbc_lblPriceOfCopy = new GridBagConstraints();
		gbc_lblPriceOfCopy.anchor = GridBagConstraints.WEST;
		gbc_lblPriceOfCopy.insets = new Insets(0, 0, 5, 5);
		gbc_lblPriceOfCopy.gridx = 0;
		gbc_lblPriceOfCopy.gridy = 3;
		contentPane.add(lblPriceOfCopy, gbc_lblPriceOfCopy);
		
		txtPriceOfCopy = new JTextField();
		GridBagConstraints gbc_txtPriceOfCopy = new GridBagConstraints();
		gbc_txtPriceOfCopy.insets = new Insets(0, 0, 5, 0);
		gbc_txtPriceOfCopy.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtPriceOfCopy.gridx = 2;
		gbc_txtPriceOfCopy.gridy = 3;
		contentPane.add(txtPriceOfCopy, gbc_txtPriceOfCopy);
		txtPriceOfCopy.setColumns(10);
		
		btnAdd = new JButton("Add");
		GridBagConstraints gbc_btnAdd = new GridBagConstraints();
		gbc_btnAdd.gridwidth = 2;
		gbc_btnAdd.gridx = 1;
		gbc_btnAdd.gridy = 5;
		contentPane.add(btnAdd, gbc_btnAdd);
		
		addActionListener();
	}
	
	private void addActionListener() {
		btnAdd.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				if (txtBookNumber.getText().equals("") || txtPriceOfCopy.getText().equals("")) {
					JOptionPane.showMessageDialog(null, "Bạn nhập thiếu dữ liệu");
					return;
				}
				sendCopyInfo();
			}
		});
	}
	
	private void sendCopyInfo() {
		ManageCopyController.addNewCopy(txtBookNumber.getText(), cbbTypeOfCopy.getSelectedItem().toString(),
				txtPriceOfCopy.getText(), this);
	}

}
