/*
 * Bản quyền thuộc về Lê Đình Phúc - MSSV: 20132979
 */
package function.boundary.librarian;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.KeyEvent;
import java.sql.SQLException;
import java.util.Vector;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

import function.controller.librarian.AcceptBorrowBookController;
import function.entity.HistoryBorrowing;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JTable;
import java.awt.Color;

/**
 * Class này là lớp giao diện thực hiện chức năng duyệt cho mượn sách.
 * @author Lê Đình Phúc
 */
public class AcceptBorrowBookUI extends JFrame {

	private JPanel contentPane;
	private JTextField tfCardID;
	private JTable table;
	private JTextField tfDayReturn;
	private JTextField tfNote;
	private JLabel lblDaMuon;
	private JLabel lblNgayHetHan;
	public AcceptBorrowBookController controller;
	private JTextField tfIDRequest;

	
//	/**
//	 * Launch the application.
//	 */
//	public static void main(String[] args) {
//		EventQueue.invokeLater(new Runnable() {
//			public void run() {
//				try {
//					AcceptBorrowBookUI frame = new AcceptBorrowBookUI();
//					frame.setVisible(true);
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//			}
//		});
//	}

	/**
	 * Create the frame.
	 */
	public AcceptBorrowBookUI() {
//		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1062, 580);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblInputBorrowerCard = new JLabel("Borrower Card ID");
		lblInputBorrowerCard.setBounds(27, 30, 173, 16);
		contentPane.add(lblInputBorrowerCard);
		
		tfCardID = new JTextField();
		tfCardID.setBounds(195, 25, 242, 26);
		contentPane.add(tfCardID);
		tfCardID.setColumns(10);
		
		JButton btnSubmit = new JButton("Tìm kiếm");
		btnSubmit.setBounds(442, 25, 117, 29);
		contentPane.add(btnSubmit);
		
		btnSubmit.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				btnSubmitActionPerformed(evt);
            }
        });
		btnSubmit.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
            	btnSubmitKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
            	btnSubmitKeyReleased(evt);
            }
        });
		
		
		
		
		String colsName[] = { "ID", "Ngày mượn", "BookID", "Tên sách", "Tác giả", "Thứ tự bản copy", "Tình trạng cuốn sách" };
		table = new JTable();
		table.setBounds(26, 154, 1011, 170);
		contentPane.add(table);
		
		JLabel lblSSch = new JLabel("Số sách đã mượn chưa trả:");
		lblSSch.setBounds(27, 72, 206, 16);
		contentPane.add(lblSSch);
		
		lblDaMuon = new JLabel("");
		lblDaMuon.setBounds(267, 72, 61, 16);
		contentPane.add(lblDaMuon);
		
		JLabel lblNgyThMn = new JLabel("Ngày thẻ mượn hết hạn:");
		lblNgyThMn.setBounds(27, 102, 173, 16);
		contentPane.add(lblNgyThMn);
		
		lblNgayHetHan = new JLabel("");
		lblNgayHetHan.setBounds(267, 102, 206, 16);
		contentPane.add(lblNgayHetHan);
		
		JLabel lblMCopy = new JLabel("ID yêu cầu mượn sách:");
		lblMCopy.setBounds(24, 348, 148, 16);
		contentPane.add(lblMCopy);
		
		JLabel lblNgayTra = new JLabel("Ngày dự kiến trả:");
		lblNgayTra.setBounds(24, 386, 148, 16);
		contentPane.add(lblNgayTra);
		
		tfDayReturn = new JTextField();
		tfDayReturn.setBounds(195, 376, 311, 26);
		contentPane.add(tfDayReturn);
		tfDayReturn.setColumns(10);
		
		JLabel lblGhiCh = new JLabel("Ghi chú:");
		lblGhiCh.setBounds(24, 423, 61, 16);
		contentPane.add(lblGhiCh);
		
		tfNote = new JTextField();
		tfNote.setBounds(195, 418, 311, 26);
		contentPane.add(tfNote);
		tfNote.setColumns(10);
		
		JButton btnXcNhnCho = new JButton("Xác nhận cho mượn");
		btnXcNhnCho.setBounds(105, 477, 163, 29);
		contentPane.add(btnXcNhnCho);
		
		JButton btnXcNhnKhng = new JButton("Xác nhận không cho mượn");
		btnXcNhnKhng.setForeground(Color.RED);
		btnXcNhnKhng.setBounds(299, 477, 207, 29);
		contentPane.add(btnXcNhnKhng);
		
		tfIDRequest = new JTextField();
		tfIDRequest.setBounds(195, 336, 173, 26);
		contentPane.add(tfIDRequest);
		tfIDRequest.setColumns(10);
		btnXcNhnKhng.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				btnXcNhnKhngActionPerformed(evt);
            }
        });
		btnXcNhnKhng.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
            	btnXcNhnKhngKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
            	btnXcNhnKhngKeyReleased(evt);
            }
        });
		btnXcNhnCho.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				btnXcNhnChoActionPerformed(evt);
            }
        });
		btnXcNhnCho.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
            	btnXcNhnChoKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
            	btnXcNhnChoKeyReleased(evt);
            }
        });
		
		
	}
	/**
    * Hàm này để sử lý sự kiện người dùng ấn vào button Xác nhận không cho mượn sách.
    * @param evt: Sự kiện ấn button.
    */ 
	private void btnXcNhnKhngActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLoginActionPerformed
		String u = this.tfIDRequest.getText().toLowerCase();
    	if(u.isEmpty()){
            JOptionPane.showMessageDialog(this,"ID yêu cầu không được để trống!");
        	return;
        }
        this.controller.denyRequest(u);
		getDataToTable();
    }//GEN-LAST:event_btnLoginActionPerformed
	/**
	* Hàm này để sử lý sự kiện người dùng sử dụng phín enter để Xác nhận không cho mượn sách.
    * @param evt: Sự kiện ấn phím enter.
    */
	private void btnXcNhnKhngKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnLoginKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_ENTER){
        	String u = this.tfIDRequest.getText().toLowerCase();
        	if(u.isEmpty()){
                JOptionPane.showMessageDialog(this,"ID yêu cầu không được để trống!");
            	return;
            }
            this.controller.denyRequest(u);
    		getDataToTable();
        }
    }//GEN-LAST:event_btnLoginKeyPressed
	private void btnXcNhnKhngKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnLoginKeyReleased
    }//GEN-LAST:event_btnLoginKeyReleased
	
	
	/**
    * Hàm này để sử lý sự kiện người dùng ấn vào button Xác nhận cho mượn sách.
    * @param evt: Sự kiện ấn button.
    */
	private void btnXcNhnChoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLoginActionPerformed
		String u = this.tfIDRequest.getText().toLowerCase();
    	String p = this.tfDayReturn.getText().toLowerCase();
		String n = this.tfNote.getText().toLowerCase();
    	if(u.isEmpty()){
            JOptionPane.showMessageDialog(this,"ID yêu cầu không được để trống!");
        	return;
        }
		if(p.isEmpty()){
            JOptionPane.showMessageDialog(this,"Ngày dự định trả yêu cầu không được để trống!");
        	return;
        }
        this.controller.acceptRequest(u,p,n);
		getDataToTable();
    }//GEN-LAST:event_btnLoginActionPerformed
	/**
	* Hàm này để sử lý sự kiện người dùng sử dụng phín enter để Xác nhận cho mượn sách.
    * @param evt: Sự kiện ấn phím enter.
    */
	private void btnXcNhnChoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnLoginKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_ENTER){
        	String u = this.tfIDRequest.getText().toLowerCase();
        	String p = this.tfDayReturn.getText().toLowerCase();
    		String n = this.tfNote.getText().toLowerCase();

    		if(u.isEmpty()){
                JOptionPane.showMessageDialog(this,"ID yêu cầu không được để trống!");
            	return;
            }
    		if(p.isEmpty()){
                JOptionPane.showMessageDialog(this,"Ngày dự định trả yêu cầu không được để trống!");
            	return;
            }
            this.controller.acceptRequest(u,p,n);
    		getDataToTable();
        }
    }//GEN-LAST:event_btnLoginKeyPressed
	private void btnXcNhnChoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnLoginKeyReleased
    }//GEN-LAST:event_btnLoginKeyReleased
	
	/**
    * Hàm này để sử lý sự kiện người dùng ấn vào button Xác nhận tìm kiếm yêu cầu mượn sách.
    * @param evt: Sự kiện ấn button.
    */
	private void btnSubmitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLoginActionPerformed
        getDataToTable();
    }//GEN-LAST:event_btnLoginActionPerformed
	/**
    * Hàm này để sử lý sự kiện người dùng ấn vào button Xác nhận tìm kiếm yêu cầu mượn sách.
    * @param evt: Sự kiện ấn phím enter.
    */	
	private void btnSubmitKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnLoginKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_ENTER){
        	getDataToTable();
        }
    }//GEN-LAST:event_btnLoginKeyPressed
	private void btnSubmitKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnLoginKeyReleased
    }//GEN-LAST:event_btnLoginKeyReleased
	
	/**
	* Hàm này để sử lý sự hiển thị kết quả tìm kiếm được yêu cầu mượn sách của người dùng.
    */
	private void getDataToTable(){
		this.lblDaMuon.setText("");
		this.tfIDRequest.setText("");
		this.lblNgayHetHan.setText("");
    	String u = tfCardID.getText().toLowerCase();
    	if(u.isEmpty()){
            JOptionPane.showMessageDialog(this,"ID của borrowr card không được để trống!");
        	return;
        }
    	try {
    		if(this.controller.checkBorrowerCardID(u) == false){
                JOptionPane.showMessageDialog(this,"ID của borrowr card không tồn tại trong hệ thống.");
                return;
    		}
    		
//    		Vector tableRecords = new Vector();//Vector chứa các dòng dữ liệu của bảng.
//    	    Vector tableTitle = new Vector();//Vector chứa các tiêu đề của bảng.
//    	    tableTitle.add("ID lượt mượn");
//            tableTitle.add("Ngày mượn");
//            tableTitle.add("Book ID");
//            tableTitle.add("Tên sách");
//            tableTitle.add("Tác giả");
//            tableTitle.add("Thứ tự bản copy");
//            tableTitle.add("Tình trạng cuốn sách");
//    	    
//    	    
//            table.setModel(new DefaultTableModel(tableRecords, tableTitle));//Set dữ liệu cho bảng

			String[][] list = this.controller.getRequestBorrower(u);
			String colsName[] = { "ID", "Ngày mượn", "BookID", "Tên sách", "Tác giả", "Thứ tự bản copy", "Tình trạng cuốn sách" };	        
			DefaultTableModel model = new DefaultTableModel(list, colsName);
	        table.setModel(model);

	        this.lblDaMuon.setText(String.valueOf(this.controller.getNumberUnpaidBook(u)));
	        this.lblNgayHetHan.setText(this.controller.getExprireDayBorrowCard(u));
	        
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
	}
}
