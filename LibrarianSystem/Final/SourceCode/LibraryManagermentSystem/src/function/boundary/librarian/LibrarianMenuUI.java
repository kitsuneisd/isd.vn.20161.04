package function.boundary.librarian;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import function.controller.librarian.AcceptBorrowBookController;

import javax.swing.JButton;
import java.awt.Color;

public class LibrarianMenuUI extends JFrame {

	private JPanel contentPane;

//	/**
//	 * Launch the application.
//	 */
//	public static void main(String[] args) {
//		EventQueue.invokeLater(new Runnable() {
//			public void run() {
//				try {
//					LibrarianMenuUI frame = new LibrarianMenuUI();
//					frame.setVisible(true);
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//			}
//		});
//	}

	/**
	 * Create the frame.
	 */
	public LibrarianMenuUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 552, 332);
		contentPane = new JPanel();
		contentPane.setForeground(Color.RED);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnAcceptBorrowBook = new JButton("Accept borrow book");
		btnAcceptBorrowBook.setBounds(19, 66, 226, 29);
		contentPane.add(btnAcceptBorrowBook);
		
		btnAcceptBorrowBook.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				btnAcceptBorrowBookActionPerformed(evt);
            }
        });
		btnAcceptBorrowBook.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
            	btnAcceptBorrowBookKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
            	btnAcceptBorrowBookKeyReleased(evt);
            }
        });
		
		
		JButton btnAcceptReturnBook = new JButton("Accept return book");
		btnAcceptReturnBook.setBounds(19, 117, 226, 29);
		contentPane.add(btnAcceptReturnBook);
		btnAcceptReturnBook.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				new AcceptReturnBookUI().setVisible(true);
			}
		});
		
		
		
		
		JButton btnGetActivateCode = new JButton("Get activate code");
		btnGetActivateCode.setBounds(19, 174, 226, 29);
		contentPane.add(btnGetActivateCode);
		
		JButton btnManageBook = new JButton("Manage book");
		btnManageBook.setBounds(304, 66, 226, 29);
		contentPane.add(btnManageBook);
		
		JButton btnManageBookCopy = new JButton("Manage book copy");
		btnManageBookCopy.setBounds(304, 117, 226, 29);
		contentPane.add(btnManageBookCopy);
		
		btnManageBookCopy.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				new ManageCopyUI().setVisible(true);
			}
		});
		
		JButton btnManageBorrower = new JButton("Manage borrower");
		btnManageBorrower.setBounds(304, 174, 226, 29);
		contentPane.add(btnManageBorrower);
		
		JButton btnLogOut = new JButton("Exit");
		btnLogOut.setForeground(Color.RED);
		btnLogOut.setBounds(223, 256, 117, 29);
		contentPane.add(btnLogOut);
		btnLogOut.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				System.exit(0);            }
        });
		btnLogOut.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
            	System.exit(0);            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
            	btnAcceptBorrowBookKeyReleased(evt);
            }
        });
	}

	private void btnAcceptBorrowBookActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLoginActionPerformed
        AcceptBorrowBookController controller = new AcceptBorrowBookController();
    }//GEN-LAST:event_btnLoginActionPerformed
	private void btnAcceptBorrowBookKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnLoginKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_ENTER){
            AcceptBorrowBookController controller = new AcceptBorrowBookController();
        }
    }//GEN-LAST:event_btnLoginKeyPressed
	private void btnAcceptBorrowBookKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnLoginKeyReleased

    }//GEN-LAST:event_btnLoginKeyReleased
	
	
}
