package function.boundary.user;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.JTable;

public class RegisterBorrowBookUI extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTable table;

//	/**
//	 * Launch the application.
//	 */
//	public static void main(String[] args) {
//		EventQueue.invokeLater(new Runnable() {
//			public void run() {
//				try {
//					RegisterBorrowBookUI frame = new RegisterBorrowBookUI();
//					frame.setVisible(true);
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//			}
//		});
//	}

	/**
	 * Create the frame.
	 */
	public RegisterBorrowBookUI() {
		setBounds(100, 100, 828, 426);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		textField = new JTextField();
		textField.setBounds(149, 41, 645, 26);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JLabel lblSearch = new JLabel("Search book");
		lblSearch.setBounds(22, 46, 141, 16);
		contentPane.add(lblSearch);
		
		JRadioButton rdbtnTitle = new JRadioButton("Title");
		rdbtnTitle.setBounds(22, 91, 88, 23);
		contentPane.add(rdbtnTitle);
		
		JRadioButton rdbtnAuthor = new JRadioButton("Author");
		rdbtnAuthor.setBounds(22, 126, 88, 23);
		contentPane.add(rdbtnAuthor);
		
		JRadioButton rdbtnPublicser = new JRadioButton("Publisher");
		rdbtnPublicser.setBounds(22, 161, 120, 23);
		contentPane.add(rdbtnPublicser);
		
		JRadioButton rdbtnNewRadioButton = new JRadioButton("Classification");
		rdbtnNewRadioButton.setBounds(22, 194, 141, 23);
		contentPane.add(rdbtnNewRadioButton);
		
		table = new JTable();
		table.setBounds(166, 96, 628, 255);
		contentPane.add(table);
	}
}
