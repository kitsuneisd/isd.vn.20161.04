package function.boundary.user;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;

public class BorrowerMenuUI extends JFrame {

	private JPanel contentPane;

//	/**
//	 * Launch the application.
//	 */
//	public static void main(String[] args) {
//		EventQueue.invokeLater(new Runnable() {
//			public void run() {
//				try {
//					BorrowerMenuUI frame = new BorrowerMenuUI();
//					frame.setVisible(true);
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//			}
//		});
//	}

	/**
	 * Create the frame.
	 */
	public BorrowerMenuUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 232, 354);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnActivateAccount = new JButton("Activate account");
		btnActivateAccount.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnActivateAccount.setBounds(6, 49, 220, 29);
		contentPane.add(btnActivateAccount);
		
		JButton btnRegisterBorrowBook = new JButton("Register borrow book");
		btnRegisterBorrowBook.setBounds(6, 102, 220, 29);
		contentPane.add(btnRegisterBorrowBook);
		btnRegisterBorrowBook.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				RegisterBorrowBookUI frame = new RegisterBorrowBookUI();
				frame.setVisible(true);				}
        });
		btnRegisterBorrowBook.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
            	RegisterBorrowBookUI frame = new RegisterBorrowBookUI();
				frame.setVisible(true);            	}
            public void keyReleased(java.awt.event.KeyEvent evt) {
            }
        });
		
		JButton btnSearchBook = new JButton("Search book");
		btnSearchBook.setBounds(6, 156, 220, 29);
		contentPane.add(btnSearchBook);
		
		JButton btnLogOut = new JButton("Exit");
		btnLogOut.setForeground(Color.RED);
		btnLogOut.setBounds(58, 247, 117, 29);
		contentPane.add(btnLogOut);
		btnLogOut.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				System.exit(0);            }
        });
		btnLogOut.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
            	System.exit(0);            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
            }
        });
	}

}
