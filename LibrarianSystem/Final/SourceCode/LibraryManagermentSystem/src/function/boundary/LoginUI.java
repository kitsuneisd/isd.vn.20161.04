/*
 * Bản quyền thuộc về Lê Đình Phúc - MSSV: 20132979
 */

package function.boundary;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.KeyEvent;
import java.sql.SQLException;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import common.Constants;
import function.controller.*;


import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.Color;
/**
 * Class này là lớp giao diện thực hiện chức năng login.
 * @author Lê Đình Phúc
 */
public class LoginUI extends JFrame {

	private JPanel contentPane;
	private JTextField tfUserName;
	private JTextField tfPassword;
    static boolean canPress=false;
	JLabel lblWanning = new JLabel("");
	public LoginController loginController;
	
//	/**
//	 * Launch the application.
//	 */
//	public static void main(String[] args) {
//		EventQueue.invokeLater(new Runnable() {
//			public void run() {
//				try {
//					LoginUI frame = new LoginUI();
//					frame.setVisible(true);
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//			}
//		});
//	}

	/**
	 * Create the frame.
	 */
	public LoginUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 438, 220);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblEmailOrUser = new JLabel("Email or user name:");
		lblEmailOrUser.setBounds(29, 25, 138, 16);
		contentPane.add(lblEmailOrUser);
		
		JLabel lblPassword = new JLabel("Password:");
		lblPassword.setBounds(29, 53, 138, 16);
		contentPane.add(lblPassword);
		
		tfUserName = new JTextField();
		tfUserName.setBounds(190, 20, 239, 26);
		contentPane.add(tfUserName);
		tfUserName.setColumns(10);
		
		tfPassword = new JTextField();
		tfPassword.setBounds(190, 48, 239, 26);
		contentPane.add(tfPassword);
		tfPassword.setColumns(10);
		
		JButton btnLogin = new JButton("Login");
		btnLogin.setBounds(168, 90, 117, 29);
		contentPane.add(btnLogin);
		
		JButton btnRegisterNewAccount = new JButton("Register new account");
		btnRegisterNewAccount.setBounds(120, 159, 229, 29);
		contentPane.add(btnRegisterNewAccount);
		
		lblWanning.setForeground(Color.RED);
		lblWanning.setBounds(53, 131, 341, 16);
		contentPane.add(lblWanning);
		
		
		btnLogin.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLoginActionPerformed(evt);
            }
        });
	    btnLogin.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnLoginKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                btnLoginKeyReleased(evt);
            }
        });
	    this.canPress = true;
	}
	
	private void btnLoginActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLoginActionPerformed
        login();
    }//GEN-LAST:event_btnLoginActionPerformed
	private void btnLoginKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnLoginKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_ENTER&&canPress){
            login();
        }
    }//GEN-LAST:event_btnLoginKeyPressed
	private void btnLoginKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnLoginKeyReleased

    }//GEN-LAST:event_btnLoginKeyReleased
    private void login(){
    	String u = tfUserName.getText().toLowerCase();
        String p = tfPassword.getText().toLowerCase();
        boolean check = true;
        System.out.println(u+"|"+p);
        if(check&u.isEmpty()){
        	lblWanning.setText("Error: email must have.");
        	return;
        } else if (check&p.isEmpty()){
    	    lblWanning.setText("Error: password must have");
    	    return;
        } else if (check){
    	    lblWanning.setText("");
            try {
            	switch (loginController.checkLogin(u, p)){
                    case Constants.LOGIN_BLOCKED:
                        JOptionPane.showMessageDialog(this,"Account is locked");
                        break;
                    case Constants.LOGIN_CHANGE_PASSWORD:
                        canPress=false;
                        canPress=true;
                        break;
                    case Constants.LOGIN_SUCCESS:
                    	this.loginController.performMenuScreen();
                        this.dispose();
                        break;
                    case Constants.LOGIN_WRONG_PASSWORD:
                        canPress=false;
                        JOptionPane.showMessageDialog(this,"Password is wrong");
                        canPress=true;
                        break;
                    default:
                        canPress=false;
                        JOptionPane.showMessageDialog(this,"Email is not exist");
                        canPress=true;
                }
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(this, "Occur error when connect database","Error!", JOptionPane.ERROR_MESSAGE);
            } catch (ClassNotFoundException ex) {
                JOptionPane.showMessageDialog(this, "Occur error durring run: "+ex.toString(),"Error!", JOptionPane.ERROR_MESSAGE);
            }
        }
    }
}
