/*
 * Bản quyền thuộc về TS. Nguyễn Thị Thu Trang, Giảng viên Viện CNTT-TT, Trường ĐHBK Hà Nội
 * Đã được chỉnh sửa bởi Lê Đình Phúc.
 * Đây là mã nguồn mẫu được tạo ra nhằm hỗ trợ sinh viên trong các lớp học của TS. N.T.T.Trang 
 * Nếu bất kỳ ai sử dụng hoặc sửa đổi ngoài mục đích học tập trên lớp học của cô Trang, cần có sự cho phép của TS. N.T.T.Trang .
 * Bất kỳ việc sử dụng hoặc sửa đổi chưa được phép đều bị coi là vi phạm bản quyền.
 */
package function.boundary;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import org.sqlite.SQLiteConfig;

/**
 * Class này giúp tạo hoặc đóng kết nối với cơ sở dữ liệu
 * @author CoTrang-Lecture
 */
public class DataAccessHelper {

    private final String GET_COUNT = "select count(*) from ";
    private static String dbPath = "jdbc:sqlite:db"+File.separator+"LibraryDB.sqlite";
    public static Connection conn = null;
    
    /**
    *Hàm này để tạo kết nối với cơ sở dữ liệu
     * @throws ClassNotFoundException
     * @throws SQLException
     * @see ClassNotFoundException
     * @see SQLException
    */
    public void connectDB() throws SQLException, ClassNotFoundException {
    	System.out.println("Mở connection.");
            Class.forName("org.sqlite.JDBC");
            SQLiteConfig config = new SQLiteConfig();  
            config.enforceForeignKeys(true);  
            conn = DriverManager.getConnection(dbPath,config.toProperties());
            conn = DriverManager.getConnection(dbPath);
            Statement stmt = conn.createStatement();
            stmt.execute("PRAGMA foreign_keys = ON");
    }
    /**
    *Hàm này để đóng kết nối cơ sở dữ liệu
     * @throws SQLException
     * @see ClassNotFoundException
     * @see SQLException
    */
    public void closeDB() throws SQLException {
        if (conn != null){
        	System.out.println("Đóng connection.");
        	conn.close();
        	conn = null;
        }
    }
    /**
    * Hàm này để đếm tổng số bản ghi của 1 bảng trong cơ sở dữ liệu
    * @param table là tên bảng cần thao tác 
    * @return int là số bản ghi trả về
     * @throws ClassNotFoundException
     * @throws SQLException
     * @see ClassNotFoundException
     * @see SQLException
    */
    public int countRecord(String table) throws SQLException, ClassNotFoundException{
        int numberOfRows=0;
        connectDB();
        PreparedStatement ps = conn.prepareStatement(GET_COUNT+table);
        ResultSet rs = ps.executeQuery();
        while  (rs.next()) {
           numberOfRows = rs.getInt(1);
        }
        closeDB();
        return numberOfRows;
    }
   
    /**
     * Hàm này để thực hiện 1 câu truy vấn lấy dữ liệu trong cơ sở dữ liệu
     * @param query: câu truy vấn
     * @return ResultSet: Dữ liệu lấy được sau khi truy vấn.
      * @throws ClassNotFoundException
      * @throws SQLException
      * @see ClassNotFoundException
      * @see SQLException
     */
     public ResultSet get(String query) throws SQLException, ClassNotFoundException{
    	 connectDB();
         PreparedStatement ps = conn.prepareStatement(query);
         ResultSet rs = ps.executeQuery();
         closeDB();
         return rs;
     }
     
     /**
      * Hàm này để thực hiện 1 câu truy vấn lưu 1 dòng dữ liệu mới vào cơ sở dữ liệu
      * @param query: câu truy vấn
       * @throws ClassNotFoundException
       * @throws SQLException
       * @see ClassNotFoundException
       * @see SQLException
      */
      public void post(String query) throws SQLException, ClassNotFoundException{
     	 connectDB();
          PreparedStatement ps = conn.prepareStatement(query);
          ps.executeUpdate();
          closeDB();
      }
     
      /**
       * Hàm này để thực hiện 1 câu truy vấn chỉnh sửa 1 dòng dữ liệu trong cơ sở dữ liệu
       * @param query: câu truy vấn
        * @throws ClassNotFoundException
        * @throws SQLException
        * @see ClassNotFoundException
        * @see SQLException
       */
       public void put(String query) throws SQLException, ClassNotFoundException{
    	   connectDB();
           PreparedStatement ps = conn.prepareStatement(query);
           ps.executeUpdate();
           closeDB();
       }
     
       /**
        * Hàm này để thực hiện 1 câu truy vấn delete 1 dòng dữ liệu trong cơ sở dữ liệu
        * @param query: câu truy vấn
         * @throws ClassNotFoundException
         * @throws SQLException
         * @see ClassNotFoundException
         * @see SQLException
        */
        public void delete(String query) throws SQLException, ClassNotFoundException{
     	   connectDB();
            PreparedStatement ps = conn.prepareStatement(query);
            ps.executeUpdate();
            closeDB();
        }
       
       
       
    public void  setDbPath(String dbPath){
        this.dbPath=dbPath;
    }
}
