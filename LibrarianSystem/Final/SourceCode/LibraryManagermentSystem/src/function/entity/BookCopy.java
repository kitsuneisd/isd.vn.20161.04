/*
 * Bản quyền thuộc về Lê Đình Phúc - MSSV: 20132979
 */
package function.entity;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import function.boundary.DataAccessHelper;

/**
 * Class này quản lý các thông tin liên quan đến BookCopy trong cơ sở dữ liệu.
 * 
 * @author Lê Đình Phúc
 */
public class BookCopy extends DataAccessHelper {
	private final String GET_BOOK_COPY = "select * from BookCopy where BookID = ? and CopySequent = ?";
	private final String UPDATE_STATE = "update BookCopy set State = ? where BookID = ? and CopySequent = ?";
	private final String GET_MAX_COPY_SEQUENT = "SELECT CopySequent FROM BookCopy"
			+ " WHERE BookID = ? ORDER BY CopySequent DESC LIMIT 0, 1";
	private final String INSERT_COPY = "INSERT INTO BookCopy VALUES (?,?,?,?,?,?)";

	private String bookID;
	private int copySequent;
	private String typeOfCopy;
	private int price;
	private String status;
	private int state;

	/**
	 * Get thông tin sách copy từ cơ sở dữ liệu.
	 * 
	 * @param id:
	 *            book id, seq: sequent book copy.
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 * @see ClassNotFoundException
	 * @see SQLException
	 */
	public void getBookCopyInfoFromDatabase(String id, String seq) throws SQLException, ClassNotFoundException {
		connectDB();
		PreparedStatement ps = conn.prepareStatement(GET_BOOK_COPY);
		ps.setString(1, id);
		ps.setString(2, seq);
		ps.executeQuery();
		ResultSet rs = ps.executeQuery();
		if (rs != null && rs.next()) {
			this.bookID = rs.getString("BookID");
			this.copySequent = rs.getInt("CopySequent");
			this.typeOfCopy = rs.getString("TypeOfCopy");
			this.price = rs.getInt("Price");
			this.status = rs.getString("Status");
			this.state = rs.getInt("State");
			closeDB();
			return;
		}
		closeDB();
		return;
	}

	/**
	 * Cập nhật trạng thái của bản copy.
	 * 
	 * @param state:
	 *            trạng thái muốn chuyển thành.
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 * @see ClassNotFoundException
	 * @see SQLException
	 */
	public void updateState(int state) throws SQLException, ClassNotFoundException {
		connectDB();
		PreparedStatement ps = conn.prepareStatement(UPDATE_STATE);
		ps.setInt(1, state);
		ps.setString(2, this.bookID);
		ps.setLong(3, this.copySequent);
		ps.executeUpdate();
		closeDB();
	}

	/**
	 * Hàm này để lấy ra trạng thái của bản copy.
	 * 
	 * @return String là tên sách.
	 */
	public String getStatus() {
		return this.status;
	}

	/**
	 * Hàm này để gán giá trị cho trạng thái của bản copy.
	 * 
	 * @param status:
	 *            tên của cuốn sách.
	 */
	public void setTitle(String status) {
		this.status = status;
	}

	/**
	 * Cập nhật trạng thái của bản copy.
	 * 
	 * @param state,
	 *            bookID, copySequent.
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 * @see ClassNotFoundException
	 * @see SQLException
	 */
	public void updateState(int state, String bookID, int copySenquent) throws SQLException, ClassNotFoundException {
		connectDB();
		PreparedStatement ps = conn.prepareStatement(UPDATE_STATE);
		ps.setInt(1, state);
		ps.setString(2, bookID);
		ps.setLong(3, copySequent);
		ps.executeUpdate();
		closeDB();
	}

	public int getMaxCopySequent(String bookID) throws ClassNotFoundException, SQLException {
		int copySequent;
		connectDB();
		PreparedStatement ps = conn.prepareStatement(GET_MAX_COPY_SEQUENT);
		ps.setString(1, bookID);
		ps.executeQuery();
		ResultSet rs = ps.executeQuery();
		if (rs == null) {
			return -1;
		}
		if (rs.next()) {
			copySequent = rs.getInt("CopySequent");
		} else {
			copySequent = 0;
		}
		closeDB();
		return copySequent;
	}
	
	public void saveNewCopy(String bookID, int copySequent, String type, int price, int state) throws ClassNotFoundException, SQLException {
		connectDB();
		PreparedStatement ps = conn.prepareStatement(INSERT_COPY);
		ps.setString(1, bookID);
		ps.setInt(2, copySequent);
		ps.setString(3, type);
		ps.setInt(4, price);
		ps.setString(5, "");
		ps.setInt(6, state);
		ps.executeUpdate();
		closeDB();
	}
}
