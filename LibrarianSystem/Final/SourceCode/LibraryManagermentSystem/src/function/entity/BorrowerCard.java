/*
 * Bản quyền thuộc về Lê Đình Phúc - MSSV: 20132979
 */
package function.entity;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import function.boundary.DataAccessHelper;
/**
 * Class này quản lý các thông tin liên quan đến borrowerCard trong cơ sở dữ liệu.
 * @author Lê Đình Phúc
 */
public class BorrowerCard extends DataAccessHelper {
    private final String SEARCH_BORROWER_CARD = "select * from BorrowerCard where BorrowerCardID = ?";
    
    private int borrowerCardID;
    private int userID;
    private String exprireDate;
	
	
	
	/**
     *Tìm người borrowerCard theo ID
     * @param id IDBorrowerCard
     *@return boolean nếu card tồn tại return true, ngược lại thì return false;
      * @throws ClassNotFoundException
      * @throws SQLException
      * @see ClassNotFoundException
      * @see SQLException
      */ 
     public boolean findCard(String id) throws SQLException, ClassNotFoundException{
         connectDB();
         PreparedStatement ps = conn.prepareStatement(SEARCH_BORROWER_CARD);
         ps.setString(1, id);
         ps.executeQuery();
         ResultSet rs = ps.executeQuery();
         if(rs !=null && rs.next()){
             closeDB();
             return true;
         }
         closeDB();
         return false;
         
     }
     /**
      *Get thông tin borrower card từ cơ sở dữ liệu.
      * @param id: id borrower card
       * @throws ClassNotFoundException
       * @throws SQLException
       * @see ClassNotFoundException
       * @see SQLException
       */ 
      public void getBorrowerCardFromDatabase(String id) throws SQLException, ClassNotFoundException{
          connectDB();
          PreparedStatement ps = conn.prepareStatement(SEARCH_BORROWER_CARD);
          ps.setString(1, id);
          ps.executeQuery();
          ResultSet rs = ps.executeQuery();
          if(rs !=null && rs.next()){
         	 this.borrowerCardID = rs.getInt("BorrowerCardID");
         	 this.userID = rs.getInt("UserID");
         	 this.exprireDate = rs.getString("ExprireDate");
              closeDB();
              return;
          }
          closeDB();
          return;
          
      }
      
      /**
       * Hàm này để lấy ra thông tin của 1 borroerCard.
       * @return String[] là UserID.
       */ 
       public String[] showInfoBorrowerCard() {
           return null;
       }
      
      /**
       * Hàm này để lấy ra idUser sở hữu borroerCard.
       * @return int là UserID.
       */ 
       public int getUserID() {
           return this.userID;
       }
       /**
       * Hàm này để gán giá trị cho userID.
       * @param id: id của user.
       */ 
       public void setUserID(int id) {
           this.userID = id;
       }
       /**
        * Hàm này để lấy ra ngày hết hạn của thẻ.
        * @return String là Ngày hết hạn của thẻ.
        */ 
        public String getExprireDate() {
            return this.exprireDate;
        }
        /**
        * Hàm này để gán giá trị ngày hết hạn cho thẻ.
        * @param date: ngày hết hạn của thẻ.
        */ 
        public void setExprireDate(String date) {
            this.exprireDate = date;
        }
}
