/*
 * Bản quyền thuộc về Lê Đình Phúc - MSSV: 20132979
 */
package function.entity;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import common.Constants;
import function.boundary.DataAccessHelper;

/**
 * Class này quản lý các thông tin liên quan đến User trong cơ sở dữ liệu.
 * @author Lê Đình Phúc
 */
public class User extends DataAccessHelper{
    private final String GET_LOGIN = "select * from User where Username=? and Password=?";
    private final String SEARCH_USER_BY_EMAIL = "select * from User where Username = ?";
    private final String SEARCH_USER_BY_ID = "select * from User where UserID = ?";

    private String username;
    private String password;
    private String firstName;
    private String lastName;
    private String birthDay;
    private String phoneNumber;
    private String gender;
    private int accountLevel;
    private String linkedAccount;
    private boolean requestToChange;
    private int  status;
    
    /**
     *Hàm này kiểm tra xem email và pass word có khớp với csdl không
     * và kiểm tra các trạng thài khóa hoặc yêu cầu thay đổi pass của người dùng.
     * @param email là email người dùng
     * @param password là mật khẩu người dùng
     *@return int return là : 0. tài khoản bị khóa, 1 yêu cầu thay đổi pass
     * 2 là đúng, 3 là sai mật khẩu, 4 là email không tồn tại.
      * @throws ClassNotFoundException
      * @throws SQLException
      * @see ClassNotFoundException
      * @see SQLException
     */
    public int checkLogin(String email, String password) throws SQLException, ClassNotFoundException {
        if(findByEmail(email)){
            connectDB();
//            System.out.println("Mở ở find/");
            PreparedStatement ps = conn.prepareStatement(GET_LOGIN);
            ps.setString(1, email);
            ps.setString(2, password);
            ResultSet rs = ps.executeQuery();
            if (rs != null && rs.next()) {
                if(rs.getInt("status") == Constants.LOGIN_BLOCKED){
                    closeDB();
                    return Constants.LOGIN_BLOCKED; // bị khóa
                }else if(rs.getInt("status") == Constants.LOGIN_CHANGE_PASSWORD){
                    closeDB();
                    return Constants.LOGIN_CHANGE_PASSWORD; // yêu cầu thay đổi mật khẩu
                }else{
                    closeDB();
                	this.getUserFromDatabase(email);
                    return Constants.LOGIN_SUCCESS; // đăng nhập thành công
                }
            }else{
                closeDB();
                return Constants.LOGIN_WRONG_PASSWORD; // sai mật khẩu
            }
        } else {
        	closeDB();
        	return Constants.LOGIN_WRONG_EMAIL; // email không tồn tại
        }
    }
    
    /**
     *Get thông tin người dùng từ cơ sở dữ liệu.
     * @param email username hoặc email tài khoản liên kết.
      * @throws ClassNotFoundException
      * @throws SQLException
      * @see ClassNotFoundException
      * @see SQLException
      */ 
     public void getUserFromDatabase(String email) throws SQLException, ClassNotFoundException{
         connectDB();
//         System.out.println("Mở ở get user/");

         PreparedStatement ps = conn.prepareStatement(SEARCH_USER_BY_EMAIL);
         ps.setString(1, email);
         ps.executeQuery();
         ResultSet rs = ps.executeQuery();
         if(rs !=null && rs.next()){
        	 this.birthDay = rs.getString("Birthday");
        	 this.username = rs.getString("Username");
        	 this.gender = rs.getString("Gender");
        	 this.phoneNumber = rs.getString("PhoneNumber");
        	 this.status = rs.getInt("Status");
        	 this.linkedAccount = rs.getString("LinkedAccounts");
        	 this.firstName = rs.getString("FirstName");
        	 this.lastName = rs.getString("LastName");
        	 this.accountLevel = rs.getInt("AccountLevel");
        	 this.password = rs.getString("Password");
             closeDB();
             return;
         } else {
         closeDB();
         return;
         }
     }
     
     /**
      *Get thông tin người dùng từ cơ sở dữ liệu.
      * @param id: là id của người dùng.
       * @throws ClassNotFoundException
       * @throws SQLException
       * @see ClassNotFoundException
       * @see SQLException
       */ 
      public void getUserByID(int id) throws SQLException, ClassNotFoundException{
          connectDB();
          PreparedStatement ps = conn.prepareStatement(SEARCH_USER_BY_ID);
          ps.setInt(1, id);
          ps.executeQuery();
          ResultSet rs = ps.executeQuery();
          if(rs !=null && rs.next()){
         	 this.birthDay = rs.getString("Birthday");
         	 this.username = rs.getString("Username");
         	 this.gender = rs.getString("Gender");
         	 this.phoneNumber = rs.getString("PhoneNumber");
         	 this.status = rs.getInt("Status");
         	 this.linkedAccount = rs.getString("LinkedAccounts");
         	 this.firstName = rs.getString("FirstName");
         	 this.lastName = rs.getString("LastName");
         	 this.accountLevel = rs.getInt("AccountLevel");
         	 this.password = rs.getString("Password");
              closeDB();
              return;
          }
          closeDB();
          return;          
      }
    
    
    /**
     *Tìm người dùng theo email
     * @param email email
     *@return boolean nếu user tồn tại return true, ngược lại thì return false;
      * @throws ClassNotFoundException
      * @throws SQLException
      * @see ClassNotFoundException
      * @see SQLException
      */ 
     public boolean findByEmail(String email) throws SQLException, ClassNotFoundException{
         connectDB();
         PreparedStatement ps = conn.prepareStatement(SEARCH_USER_BY_EMAIL);
         ps.setString(1, email);
         ps.executeQuery();
         ResultSet rs = ps.executeQuery();
         if(rs !=null && rs.next()){
             closeDB();
             return true;
         }
         closeDB();
         return false;
         
     }
     /**
      * Hàm này để lấy ra account level trong bảng User
      * @return Int là account level
      */ 
      public int getAccountLevel() {
          return this.accountLevel;
      }
      /**
      * Hàm này để gán giá trị cho account level trong bảng User
      * @param level: là account level
      */ 
      public void setRoleName(int level) {
          this.accountLevel = level;
      }
      /**
      * Hàm này để lấy ra username trong bảng User
      * @return String là username
      */ 
      public String getEmail() {
          return this.username;
      }
      /**
      * Hàm này để gán giá trị cho username trong bảng User
      * @param username là username 
      */ 
      public void setEmail(String username) {
          this.username = username;
      }
      /**
      * Hàm này để lấy ra password trong bảng User
      * @return String là password
      */ 
      public String getPassword() {
          return password;
      }
      /**
      * Hàm này để gán giá trị cho password trong bảng User
      * @param password là password
      */ 
      public void setPassword(String password) {
          this.password = password;
      }
      /**
      * Hàm này để lấy ra firstName trong bảng User
      * @return String là firstName
      */ 
      public String getFirstName() {
          return firstName;
      }
      /**
      * Hàm này để gán giá trị cho firstName trong bảng User
      * @param firstName là firstName
      */ 
      public void setFirstName(String firstName) {
          this.firstName = firstName;
      }
      /**
      * Hàm này để lấy ra lastName trong bảng User
      * @return String là lastName
      */ 
      public String getLastName() {
          return lastName;
      }
      /**
      * Hàm này để gán giá trị cho lastName trong bảng User
      * @param lastName là lastName
      */ 
      public void setLastName(String lastName) {
          this.lastName = lastName;
      }
      /**
      * Hàm này để lấy ra birthDay trong bảng User
      * @return String là birthDay
      */ 
      public String getBirthDay() {
          return birthDay;
      }
      /**
      * Hàm này để gán giá trị cho birthDay trong bảng User
      * @param birthDay là birthDay
      */ 
      public void setBirthDay(String birthDay) {
          this.birthDay = birthDay;
      }
      /**
      * Hàm này để lấy ra phoneNumber trong bảng User
      * @return String là phoneNumber
      */ 
      public String getPhoneNumber() {
          return phoneNumber;
      }
      /**
      * Hàm này để gán giá trị cho phoneNumber trong bảng User
      * @param phoneNumber là phoneNumber
      */ 
      public void setPhoneNumber(String phoneNumber) {
          this.phoneNumber = phoneNumber;
      }
      /**
      * Hàm này để lấy ra gender trong bảng User
      * @return String là gender
      */ 
      public String getGender() {
          return gender;
      }
      /**
      * Hàm này để gán giá trị cho gender trong bảng User
      * @param gender là gender
      */ 
      public void setGender(String gender) {
          this.gender = gender;
      }
      /**
      * Hàm này để lấy ra status trong bảng User
      * @return boolean là status
      */ 
      public int getStatus() {
          return status;
      }
      /**
      * Hàm này để gán giá trị cho status trong bảng User
      * @param status là status
      */ 
      public void setStatus(int status) {
          this.status = status;
      }
}
