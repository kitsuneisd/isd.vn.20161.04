/*
 * Bản quyền thuộc về Lê Đình Phúc - MSSV: 20132979
 */
package function.entity;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import common.Constants;
import function.boundary.DataAccessHelper;
import function.controller.librarian.BookCopyInfo;
import function.controller.librarian.BorrowingInfo;

/**
 * Class này quản lý các thông tin liên quan đến HistoryBorrowing trong cơ sở dữ
 * liệu.
 * 
 * @author Lê Đình Phúc
 */
public class HistoryBorrowing extends DataAccessHelper {
	private final String GET_BORROW_BOOK = "select * from HistoryBorrowing where UserID = ? and State = ?";
	private final String COUNT_BORROW_BOOK = "select count(*) from HistoryBorrowing where UserID = ? and State = ?";
	private final String GET_REQUEST_BORROW_BOOK = "select * from HistoryBorrowing where HistoryBorrowingID = ?";
	private final String UPDATE_STATE = "update HistoryBorrowing set State = ? WHERE HistoryBorrowingID = ?";
	private final String UPDATE_INTENDEDRETURN = "update HistoryBorrowing set IntendedReturnDate = ? WHERE HistoryBorrowingID = ?";
	private final String UPDATE_NOTE = "update HistoryBorrowing set Note = ? WHERE HistoryBorrowingID = ?";
	private final String GET_LENT_BOOK_BY_COPYID = "select * from HistoryBorrowing where BookID = ? and CopySenquent = ? and State = ?";
	private final String DELETE_LENT_COPY = "DELETE FROM HistoryBorrowing WHERE HistoryBorrowingID = ?";
	private int historyBorrowingID;
	private String borrowedDate;
	private String intendedReturnDate;
	private String note;
	private int state;
	private int copySenquent;
	private int userID;
	private String bookID;

	/**
	 * Từ chối yêu cầu mượn sách.
	 * 
	 * @param id: id của yêu cầu.
	 */
	public void denyRequest(String id) {
		try {
			this.getRequestBorrowBook(id);
			connectDB();
			System.out.println("ID: " + id);
			PreparedStatement ps = conn.prepareStatement(UPDATE_STATE);
	        ps.setString(1, "1");
	        ps.setString(2,id);
	        ps.executeUpdate();
			closeDB();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * Chấp thuận yêu cầu mượn sách.
	 * 
	 * @param id: id của yêu cầu. day: ngày định trả, note: ghi chú của thủ thư.
	 * 		day: ngày cho mượn
	 * 		note: ghi chú cho mượn.
	 */
	public void acceptRequest(String id, String day, String note) {
		try {
			this.getRequestBorrowBook(id);
			connectDB();
			System.out.println("ID: " + id);
			PreparedStatement ps = conn.prepareStatement(UPDATE_STATE);
			ps.setString(1, "4");
			ps.setString(2, id);
			ps.executeUpdate();
			ps = conn.prepareStatement(this.UPDATE_INTENDEDRETURN);
			ps.setString(1, day);
			ps.setString(2, id);
			ps.executeUpdate();
			ps = conn.prepareStatement(this.UPDATE_NOTE);
			ps.setString(1, note);
			ps.setString(2, id);
			ps.executeUpdate();
			closeDB();

		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * Get thông tin 1 yêu cầu mượn sách.
	 * 
	 * @param id: id của yêu cầu mượn sách.

	 * @throws ClassNotFoundException
	 * @throws SQLException
	 * @see ClassNotFoundException
	 * @see SQLException
	 */
	public void getRequestBorrowBook(String id) throws ClassNotFoundException, SQLException {
		connectDB();
		PreparedStatement ps = conn.prepareStatement(GET_REQUEST_BORROW_BOOK);
		ps.setString(1, id);
		ps.executeQuery();
		ResultSet rs = ps.executeQuery();
		if (rs != null && rs.next()) {
			this.historyBorrowingID = rs.getInt("HistoryBorrowingID");
			this.bookID = rs.getString("BookID");
			this.borrowedDate = rs.getString("BorrowedDate");
			this.copySenquent = rs.getInt("CopySenquent");
			this.userID = rs.getInt("UserID");
			this.intendedReturnDate = rs.getString("IntendedReturnDate");
			this.state = rs.getInt("State");
			this.note = rs.getString("Note");
		}
		closeDB();
	}
	/**
	 * Get thông tin cơ bản cuả 1 quyển sách sách từ cơ sở dữ liệu.
	 * 
	 * @return String[]
	 * @see ClassNotFoundException
	 * @see SQLException
	 */
	public String[] showBookBasicInfomation(){
		return null;
	}

	/**
	 * Đếm số sách người dùng vẫn đang giữ chưa trả.
	 * 
	 * @param userID:
	 *            userID của người mượn.
	 * @return int: Số lượng sách vẫn đang giữ.
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 * @see ClassNotFoundException
	 * @see SQLException
	 */
	public int countUnpaidBook(String userID) throws ClassNotFoundException, SQLException {
		connectDB();
		PreparedStatement ps = conn.prepareStatement(COUNT_BORROW_BOOK);
		ps.setString(1, userID);
		ps.setLong(2, Constants.LENT);
		ps.executeQuery();
		ResultSet rs = ps.executeQuery();
		int a = rs.getInt(1);
		closeDB();
		return a;
	}

	/**
	 * Get list danh sách yêu cầu mượn.
	 * 
	 * @param id
	 *            IDBorrowerCard
	 * @return HistoryBorrowing[] list danh sách đã đăng kí mượn.
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 * @see ClassNotFoundException
	 * @see SQLException
	 */
	public HistoryBorrowing[] getListRequestBorrowBook(int id) throws SQLException, ClassNotFoundException {
		connectDB();
		PreparedStatement ps = conn.prepareStatement(COUNT_BORROW_BOOK);
		ps.setLong(1, id);
		ps.setLong(2, Constants.BORROW);
		ps.executeQuery();
		ResultSet rs = ps.executeQuery();
		int count = rs.getInt(1);

		ps = conn.prepareStatement(GET_BORROW_BOOK);
		ps.setLong(1, id);
		ps.setLong(2, Constants.BORROW);
		ps.executeQuery();
		rs = ps.executeQuery();
		if (rs == null) {
			closeDB();
			return null;
		}
		HistoryBorrowing a[] = new HistoryBorrowing[count];
		int index = 0;
		while (rs.next()) {
			HistoryBorrowing temp = new HistoryBorrowing();
			temp.historyBorrowingID = rs.getInt("HistoryBorrowingID");
			temp.bookID = rs.getString("BookID");
			temp.borrowedDate = rs.getString("BorrowedDate");
			temp.copySenquent = rs.getInt("CopySenquent");
			temp.userID = rs.getInt("UserID");
			temp.intendedReturnDate = rs.getString("IntendedReturnDate");
			temp.state = rs.getInt("State");
			temp.note = rs.getString("Note");
			a[index] = temp;
			index++;
		}
		closeDB();
		return a;
	}

	/**
	 * Hàm này để lấy ra id của yêu cầu mượn sách của quyển sách.
	 * 
	 * @return int là id của yêu cầu.
	 */
	public int getBorrowingID() {
		return this.historyBorrowingID;
	}

	/**
	 * Hàm này để gán giá trị cho id của yêu cầu mượn sách.
	 * 
	 * @param id:
	 *            id của yêu cầu mượn sách.
	 */
	public void setBorrowingID(int id) {
		this.historyBorrowingID = id;
	}

	/**
	 * Hàm này để lấy ra ngày mượn của yêu cầu mượn sách của quyển sách.
	 * 
	 * @return String là ngày yêu cầu mượn của yêu cầu.
	 */
	public String getBorrowedDate() {
		return this.borrowedDate;
	}

	/**
	 * Hàm này để gán giá trị cho borrowedDate của yêu cầu mượn sách.
	 * 
	 * @param date: trả về ngày mượn sách.
	 */
	public void setBorrowedDate(String date) {
		this.borrowedDate = date;
	}

	/**
	 * Hàm này để lấy ra id book của yêu cầu mượn sách.
	 * 
	 * @return int là id của cuốn sách.
	 */
	public String getIDBook() {
		return this.bookID;
	}

	/**
	 * Hàm này để gán giá trị cho bookID của yêu cầu mượn sách.
	 * 
	 * @param id:
	 *            id của yêu cầu mượn sách.
	 */
	public void setBookID(String id) {
		this.bookID = id;
	}

	/**
	 * Hàm này để lấy ra note của yêu cầu mượn sách.
	 * 
	 * @return String là note của yêu cầu.
	 */
	public String getNote() {
		return this.note;
	}

	/**
	 * Hàm này để gán giá trị cho note của yêu cầu mượn sách.
	 * 
	 * @param note: String yêu cầu mượn sách.
	 */
	public void setNote(String note) {
		this.note = note;
	}
	
	
	/**
	 * Hàm này để lấy ra copy senquent của yêu cầu mượn sách.
	 * 
	 * @return int là senquent của cuốn sách.
	 */
	public int getCopySenquent() {
		return this.copySenquent;
	}

	/**
	 * Hàm này để gán giá trị cho copy senquent của yêu cầu mượn sách.
	 * 
	 * @param id:
	 *            senquent của yêu cầu mượn sách.
	 */
	public void setCopySenquent(int id) {
		this.copySenquent = id;
	}

	/**
	 * Get list danh sách sách đã mượn
	 * 
	 * @param id: UserID
	 * @return BorrowingInfo.
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 * @see ClassNotFoundException
	 * @see SQLException
	 */
	public BorrowingInfo getListLentBook(int id) throws SQLException, ClassNotFoundException {
		connectDB();

		PreparedStatement ps = conn.prepareStatement(GET_BORROW_BOOK);
		ps.setLong(1, id);
		ps.setLong(2, Constants.LENT);
		ps.executeQuery();
		ResultSet rs = ps.executeQuery();
		if (rs == null) {
			closeDB();
			return null;
		}
		BorrowingInfo bi = new BorrowingInfo();
		while (rs.next()) {
			BookCopyInfo temp = new BookCopyInfo();
			temp.setHistoryBorrowingID(rs.getInt("HistoryBorrowingID"));
			temp.setBookID(rs.getString("BookID"));
			temp.setBorrowedDate(rs.getString("BorrowedDate"));
			temp.setCopySenquent(rs.getInt("CopySenquent"));
			temp.setUserID(rs.getInt("UserID"));
			temp.setIntendedReturnDate(rs.getString("IntendedReturnDate"));
			temp.setState(rs.getInt("State"));
			temp.setNote(rs.getString("Note"));

			bi.getListBookCopy().add(temp);
		}
		closeDB();
		return bi;
	}

	/**
	 * Get sách đã mượn
	 * 
	 * @param id: CopyID
	 * @return BookCopyInfo.
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 * @see ClassNotFoundException
	 * @see SQLException
	 */
	public BookCopyInfo getLentBook(String id) throws SQLException, ClassNotFoundException {
		connectDB();
		String bookID = id.substring(0, 6);
		int copySenquent = Integer.parseInt(id.substring(6));
		PreparedStatement ps = conn.prepareStatement(GET_LENT_BOOK_BY_COPYID);
		ps.setString(1, bookID);
		ps.setInt(2, copySenquent);
		ps.setLong(3, Constants.LENT);
		ps.executeQuery();
		ResultSet rs = ps.executeQuery();
		if (rs == null) {
			closeDB();
			return null;
		}
		BookCopyInfo temp = new BookCopyInfo();
		temp.setHistoryBorrowingID(rs.getInt("HistoryBorrowingID"));
		temp.setBookID(rs.getString("BookID"));
		temp.setBorrowedDate(rs.getString("BorrowedDate"));
		temp.setCopySenquent(rs.getInt("CopySenquent"));
		temp.setUserID(rs.getInt("UserID"));
		temp.setIntendedReturnDate(rs.getString("IntendedReturnDate"));
		temp.setState(rs.getInt("State"));
		temp.setNote(rs.getString("Note"));

		closeDB();
		return temp;
	}
	
	/**
	 * Delete HistoryBorrowing sau khi Borrower tra sach
	 * 
	 * @param id: History Borrowing ID
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 * @see ClassNotFoundException
	 * @see SQLException
	 */
	public void deleteLentCopy(int id) throws ClassNotFoundException, SQLException {
		connectDB();
		PreparedStatement ps = conn.prepareStatement(DELETE_LENT_COPY);
		ps.setInt(1, id);
		ps.executeUpdate();
		closeDB();
	}
}
