/*
 * Bản quyền thuộc về Lê Đình Phúc - MSSV: 20132979
 */
package function.entity;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import function.boundary.DataAccessHelper;

/**
 * Class này quản lý các thông tin liên quan đến Book trong cơ sở dữ liệu.
 * 
 * @author Lê Đình Phúc
 */
public class Book extends DataAccessHelper {
	private final String GET_BOOK = "select * from Book where BookID = ?";

	private String bookID;
	private String title;
	private String publisher;
	private String author;
	private String ISBN;
	private String content;

	/**
	 * Get thông tin sách từ cơ sở dữ liệu.
	 * 
	 * @param id:
	 *            book id.
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 * @see ClassNotFoundException
	 * @see SQLException
	 */
	public void getBookInfoFromDatabase(String id) throws SQLException, ClassNotFoundException {
		connectDB();
		PreparedStatement ps = conn.prepareStatement(GET_BOOK);
		ps.setString(1, id);
		ps.executeQuery();
		ResultSet rs = ps.executeQuery();
		if (rs != null && rs.next()) {
			this.bookID = rs.getString("BookID");
			this.title = rs.getString("Title");
			this.publisher = rs.getString("Publisher");
			this.author = rs.getString("Author");
			this.ISBN = rs.getString("ISBN");
			this.content = rs.getString("Content");
			closeDB();
			return;
		}
		closeDB();
		return;
	}
	/**
	 * Get thông tin chi tiết cuả 1 quyển sách sách từ cơ sở dữ liệu.
	 * 
	 * @return String[]
	 * @see ClassNotFoundException
	 * @see SQLException
	 */
	public String[] showBookDetailInfomation(){
		return null;
	}
	/**
	 * Get thông tin cơ bản cuả 1 quyển sách sách từ cơ sở dữ liệu.
	 * 
	 * @return String[]
	 * @see ClassNotFoundException
	 * @see SQLException
	 */
	public String[] showBookBasicInfomation(){
		return null;
	}
	/**
	 * Hàm này để lấy ra tên của quyển sách.
	 * 
	 * @return String là tên sách.
	 */
	public String getTitle() {
		return this.title;
	}

	/**
	 * Hàm này để gán giá trị cho title trong book.
	 * 
	 * @param title:
	 *            tên của cuốn sách.
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Hàm này để lấy ra tác giả của quyển sách.
	 * 
	 * @return String là tên tác giả.
	 */
	public String getAuthor() {
		return this.author;
	}

	/**
	 * Hàm này để gán giá trị cho tác giả trong book.
	 * 
	 * @param author:
	 *            tên tác giả của cuốn sách.
	 */
	public void setAuthor(String author) {
		this.author = author;
	}

	public String getBookName(String id) {
		try {
			getBookInfoFromDatabase(id);
			return this.title;
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}
