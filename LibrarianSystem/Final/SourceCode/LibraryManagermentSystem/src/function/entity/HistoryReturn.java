package function.entity;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import function.boundary.DataAccessHelper;
import function.controller.librarian.BookCopyInfo;

public class HistoryReturn extends DataAccessHelper {
	public final String INSERT_RETURN_BOOK = "INSERT INTO HistoryReturn VALUES (?,?,?,?,?,?,?)";

	/**
	 * Insert HistoryReturn sau khi Borrower tra sach
	 * 
	 * @param bci: Book copy id.
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 * @see ClassNotFoundException
	 * @see SQLException
	 */
	public void insertHistoryReturn(BookCopyInfo bci) throws ClassNotFoundException, SQLException {
		connectDB();
		PreparedStatement ps = conn.prepareStatement(INSERT_RETURN_BOOK);
		//ps.setInt(1, 1);
		ps.setString(2, bci.getBorrowedDate());
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		ps.setString(3, sdf.format(Calendar.getInstance().getTime()));
		ps.setString(4, bci.getNote());
		ps.setInt(5, bci.getCopySenquent());
		ps.setInt(6, bci.getUserID());
		ps.setString(7, bci.getBookID());
		ps.executeUpdate();
		closeDB();
	}
}
