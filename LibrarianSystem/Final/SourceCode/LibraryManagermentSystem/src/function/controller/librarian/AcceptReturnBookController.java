package function.controller.librarian;

import java.sql.SQLException;

import javax.swing.JOptionPane;

import common.Constants;
import function.boundary.librarian.AcceptReturnBookUI;
import function.entity.BookCopy;
import function.entity.Borrower;
import function.entity.BorrowerCard;
import function.entity.HistoryBorrowing;
import function.entity.HistoryReturn;



public class AcceptReturnBookController {
	private static HistoryBorrowing hb = new HistoryBorrowing();
	
	public static int searchLentBooks(String borrowerCardID, AcceptReturnBookUI ui) {
		BorrowerCard bc = new BorrowerCard();
		
		BorrowingInfo bi;
		try {
			bc.getBorrowerCardFromDatabase(borrowerCardID);
			if (bc.getUserID() == 0) {
				JOptionPane.showMessageDialog(ui, "Không tồn tại borrower card: " + borrowerCardID);
				return 1;
			}
			
			String username = Borrower.getBorrowerName(bc.getUserID());
			bi = hb.getListLentBook(bc.getUserID());
			
			if (bi == null) {
				JOptionPane.showMessageDialog(ui, "Borrower " + username + 
						" không có bản copy nào chưa trả");
				return 1;
			}
			bi.setBorrowedName(username);
			bi.setBorrowerID(bc.getUserID());

			function.entity.Book book = new function.entity.Book();
			for (BookCopyInfo bci : bi.getListBookCopy()) {
				String bookName = book.getBookName(bci.getBookID());
				bci.setBookName(bookName);
			}
			ui.showResult(bi);
			return 0;
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 1;		
	}
	
	public static void searchLentBook(String copyID, AcceptReturnBookUI ui) {
		try {
			BookCopyInfo bci = hb.getLentBook(copyID);
			if (bci == null) {
				JOptionPane.showMessageDialog(ui, "Không tồn tại bản copy có id: "
						+ copyID + " trong danh sách bản copy cho mượn");
				return;
			}
			String username = Borrower.getBorrowerName(bci.getUserID());
			ui.showResult(bci, username);
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void updateBorrowingInfo(BorrowingInfo bi) {
		if (bi == null) {
			return;
		}
		HistoryBorrowing hb = new HistoryBorrowing();
		HistoryReturn hr = new HistoryReturn();
		BookCopy bc = new BookCopy();
		
		for (BookCopyInfo bci : bi.getListBookCopy()) {
			try {
				bc.updateState(Constants.AVAIABLE, bci.getBookID(), bci.getCopySenquent());
				hb.deleteLentCopy(bci.getHistoryBorrowingID());
				hr.insertHistoryReturn(bci);
			} catch (ClassNotFoundException | SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}