package function.controller.librarian;

public class BookCopyInfo {
	private int copySenquent;
	private String bookID;
	private String bookName;
	private int state;
	private String note;
	private String borrowedDate;
	private String intendedReturnDate;
	private int userID;
	private int historyBorrowingID;
	private boolean selected;

	
	public int getCopySenquent() {
		return copySenquent;
	}

	public void setCopySenquent(int copySenquent) {
		this.copySenquent = copySenquent;
	}

	public String getBookName() {
		return bookName;
	}

	public void setBookName(String bookName) {
		this.bookName = bookName;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getBorrowedDate() {
		return borrowedDate;
	}

	public void setBorrowedDate(String borrowedDate) {
		this.borrowedDate = borrowedDate;
	}

	public String getIntendedReturnDate() {
		return intendedReturnDate;
	}

	public void setIntendedReturnDate(String intendedReturnDate) {
		this.intendedReturnDate = intendedReturnDate;
	}

	public int getHistoryBorrowingID() {
		return historyBorrowingID;
	}

	public void setHistoryBorrowingID(int historyBorrowingID) {
		this.historyBorrowingID = historyBorrowingID;
	}

	public String getBookID() {
		return bookID;
	}

	public void setBookID(String bookID) {
		this.bookID = bookID;
	}

	public int getUserID() {
		return userID;
	}

	public void setUserID(int userID) {
		this.userID = userID;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

}
