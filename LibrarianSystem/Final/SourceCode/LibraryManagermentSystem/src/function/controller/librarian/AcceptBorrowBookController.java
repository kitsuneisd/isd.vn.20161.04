/*
 * Bản quyền thuộc về Lê Đình Phúc - MSSV: 20132979
 */
package function.controller.librarian;

import java.sql.ResultSet;
import java.sql.SQLException;

import common.Constants;
import function.boundary.DataAccessHelper;
import function.boundary.LoginUI;
import function.boundary.librarian.AcceptBorrowBookUI;
import function.entity.Book;
import function.entity.BookCopy;
import function.entity.BorrowerCard;
import function.entity.HistoryBorrowing;
import function.entity.User;

/**
 * Class này là lớp điều khiển, chức năng quyết định việc hiển thị, giao tiếp với các lớp biên và sử lý sự kiên. Thực hiện chức năng duyệt cho mượn sách.
 * @author Lê Đình Phúc
 */
public class AcceptBorrowBookController extends DataAccessHelper{
	private BorrowerCard card = new BorrowerCard();
	private HistoryBorrowing his = new HistoryBorrowing();
	private Book book = new Book();
	private AcceptBorrowBookUI accept = new AcceptBorrowBookUI();
	private BookCopy bookCopy = new BookCopy();
	
	/**
    * Hàm khỏi tạo đối tượng AcceptBorrowBookController
    */
	public AcceptBorrowBookController(){
		accept.controller = this;
		accept.setVisible(true);
	}
	/**
     *Kiểm tra thẻ mượn có tồi tại không.
     * @param cardID: id của thẻ mượn.
     *@return boolean: Nếu thẻ mượn tồn tại thì trả về true không thì ngược lại.
    */
	public boolean checkBorrowerCardID (String cardID) throws ClassNotFoundException, SQLException{
		return card.findCard(cardID);
	}
	/**
     *Lấy số lượng sách người dùng vẫn giữ chưa trả.
     * @param cardID: id của thẻ mượn.
     *@return int: số lượng sách người dùng vẫn giữ.
    */
	public int getNumberUnpaidBook(String cardID) throws ClassNotFoundException, SQLException{
		card.getBorrowerCardFromDatabase(cardID);
		int a = card.getUserID();
		return his.countUnpaidBook(String.valueOf(a));
	}
	/**
     *Lấy ra ngày thẻ mượn hết hạn.
     * @param cardID: id của thẻ mượn.
     *@return String: ngày thẻ hết hạn.
    */
	public String getExprireDayBorrowCard(String cardID) throws ClassNotFoundException, SQLException{
		return this.card.getExprireDate();
	}
	
	/**
     *Chấp thuận cho người dùng mượn sách.
     * @param id: id của yêu cầu mượn, day: ngày người mượn dự định trả, note: ghi chú của thủ thư.
    */
	public void acceptRequest(String id, String day, String note){
		try {
			his.acceptRequest(id, day, note);
			bookCopy.getBookCopyInfoFromDatabase(his.getIDBook(), String.valueOf(his.getCopySenquent()));
			bookCopy.updateState(Constants.LENT);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
     *Từ chối cho mượn sách.
     * @param id: id của yêu cầu mượn sách.
    */
	public void denyRequest(String id){
		try {
			his.denyRequest(id);
			bookCopy.getBookCopyInfoFromDatabase(his.getIDBook(), String.valueOf(his.getCopySenquent()));
			bookCopy.updateState(Constants.AVAIABLE);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
     *Lấy số yêu cầu mượn chưa được sử lý của người dùng.
     * @param cardID: id của thẻ mượn.
     *@return String[][]: Mảng 2 chiều gồm các thông tin để hiển thị.
    */
	public String[][] getRequestBorrower (String cardID) throws ClassNotFoundException, SQLException{
		card.getBorrowerCardFromDatabase(cardID);
		HistoryBorrowing a[] = his.getListRequestBorrowBook(card.getUserID());
		
		String[][] listReturn = new String[a.length][7];
		for(int i=0; i< a.length; i++){
			String data[] = new String[7];
			data[0] = String.valueOf(a[i].getBorrowingID());
			//System.out.println("Borrowing ID:" + data[0]);
			
			data[1] = a[i].getBorrowedDate();
			//System.out.println("Borrowed Date ID:" + data[1]);

			data[2] = a[i].getIDBook();
			//System.out.println("Book ID:" + data[2]);
			
			book.getBookInfoFromDatabase(data[2]);
			data[3] = book.getTitle();
			//System.out.println("Title:" + data[3]);
			
			data[4] = book.getAuthor();
			//System.out.println("Author:" + data[4]);

			data[5] = String.valueOf(a[i].getCopySenquent());
			//System.out.println("Copy sequent:" + data[5]);

			bookCopy.getBookCopyInfoFromDatabase(data[2], data[5]);
			data[6] = bookCopy.getStatus();
			//System.out.println("Copy status:" + data[6]);

			
			listReturn[i] = data;
		}
//		for(int i=0; i< a.length; i++){
//			String data[] = listReturn[i];
//			System.out.println(data[0]);
//			System.out.println(data[1]);
//			System.out.println(data[2]);
//			System.out.println(data[3]);
//			System.out.println(data[4]);
//			System.out.println(data[5]);
//			System.out.println(data[6]);
//
//		}
		return listReturn;
	}
}
