package function.controller.librarian;

import java.util.ArrayList;
import java.util.List;

public class BorrowingInfo {
	private List<BookCopyInfo> listBookCopy = new ArrayList<>();
	private String borrowedName;
	private int borrowerID;

	public List<BookCopyInfo> getListBookCopy() {
		return listBookCopy;
	}

	public void setListBookCopy(List<BookCopyInfo> listBookCopy) {
		this.listBookCopy = listBookCopy;
	}

	public String getBorrowedName() {
		return borrowedName;
	}

	public void setBorrowedName(String borrowedName) {
		this.borrowedName = borrowedName;
	}

	public int getBorrowerID() {
		return borrowerID;
	}

	public void setBorrowerID(int borrowerID) {
		this.borrowerID = borrowerID;
	}

}