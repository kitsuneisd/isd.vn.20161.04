package function.controller.librarian;

import java.sql.SQLException;

import javax.swing.JOptionPane;

import common.Constants;
import function.boundary.librarian.AddNewCopyUI;
import function.entity.Book;
import function.entity.BookCopy;

public class ManageCopyController {
	public static boolean checkBookID(String bookID) {
		Book b = new Book();
		try {
			b.getBookInfoFromDatabase(bookID);
			if (b.getTitle() != null) {
				return true;
			}
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	
	public static void addNewCopy(String bookID, String type, String price, AddNewCopyUI ui) {
		BookCopy bc = new BookCopy();
		int state;
		if (checkBookID(bookID) == false) {
			JOptionPane.showMessageDialog(ui, "Không tồn tại sách có BookID = " + bookID);
			return;
		}
		
		if (Integer.parseInt(price) < 0) {
			JOptionPane.showMessageDialog(ui, "Bạn nhập sai giá bản copy");
			return;
		}
		
		try {
			int copySequent = bc.getMaxCopySequent(bookID);
			if (type.equals("Reference")) {
				state = Constants.REFERENCED;
			} else {
				state = Constants.AVAIABLE;
			}
			bc.saveNewCopy(bookID, copySequent + 1, type, Integer.parseInt(price), state);
			JOptionPane.showMessageDialog(ui, "Bạn thêm bản copy thành công");
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
