/*
 * Bản quyền thuộc về Lê Đình Phúc - MSSV: 20132979
 */
package function.controller;

import java.sql.SQLException;

import function.entity.*;
import common.*;
import function.boundary.*;
import function.boundary.librarian.LibrarianMenuUI;
import function.boundary.user.BorrowerMenuUI;
/**
 * Class này là lớp điều khiển, chức năng quyết định việc hiển thị, giao tiếp với các lớp biên và sử lý sự kiên. Thực hiện chức năng login tài khoản.
 * @author Lê Đình Phúc
 */
public class LoginController {
	User user = new User();

	public LoginController(){
		LoginUI login = new LoginUI();
		login.loginController = this;
		login.setVisible(true);
	}
	
	/**
     *Hàm này để kiểm tra người dùng có thể đăng nhập vào hệ thống không
     * @param email là email người dùng
     * @param password  là mật khẩu người dùng
     *  *@return int return là : 0. tài khoản bị khóa, 1 yêu cầu thay đổi pass
     * 2 sai mật khẩu, 3 là đúng, 4 là email không tồn tại.
     */
    public int  checkLogin(String email,String password) throws SQLException, ClassNotFoundException{
       return  user.checkLogin(email, password);
    }
    
    /**
     *Hàm này để chuyển đến các màn hình chức năng tương ứng đối với mỗi loại người dùng.
     * 1 borrower, 2 library, 3 admin.
     */
    public void  performMenuScreen(){
    	if (this.user.getAccountLevel() == Constants.LIBRARIAN){
    		LibrarianMenuUI menu = new LibrarianMenuUI();
    		menu.setVisible(true);
    	} else if (this.user.getAccountLevel() == Constants.ADMIN){
    		
    	} else {
    		BorrowerMenuUI menu = new BorrowerMenuUI();
    		menu.setVisible(true);

    	}
    }
   
}
