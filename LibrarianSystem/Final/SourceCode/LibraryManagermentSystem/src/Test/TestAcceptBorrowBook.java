/*
 * Bản quyền thuộc về Lê Đình Phúc - MSSV: 20132979
 */
package Test;

import static org.junit.Assert.*;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.xml.parsers.ParserConfigurationException;

import org.junit.Test;

import common.Constants;
import function.boundary.DataAccessHelper;
import function.controller.librarian.AcceptBorrowBookController;
/**
 * Class này để kiểm tra các hàm trong class AcceptBorrowBookController.
 * @author Lê Đình Phúc
 */
public class TestAcceptBorrowBook extends DataAccessHelper {
	private AcceptBorrowBookController a = new AcceptBorrowBookController();
	private final String UPDATE_STATE = "update HistoryBorrowing set State = ? WHERE HistoryBorrowingID = ?";
	private final String UPDATE_INTENDEDRETURN = "update HistoryBorrowing set IntendedReturnDate = ? WHERE HistoryBorrowingID = ?";
	private final String UPDATE_NOTE = "update HistoryBorrowing set Note = ? WHERE HistoryBorrowingID = ?";
	
	private final String GET_INFO_REQEST = "select * from HistoryBorrowing where HistoryBorrowingID = ?";
	
	/**
     *  Hàm này để test chức năng kiểm tra thẻ mượn có hợp lệ không?
     * @throws SQLException
     * @throws ClassNotFoundException
     * @see ClassNotFoundException
     * @see SQLException
     * @see ParserConfigurationException
     */
	@Test
	public void testCheckBorrowerCardID() throws ClassNotFoundException, SQLException {
        assertEquals(false, a.checkBorrowerCardID("linhtinh")); 
        assertEquals(false, a.checkBorrowerCardID("3")); 
        assertEquals(true, a.checkBorrowerCardID("1")); 
        assertEquals(true, a.checkBorrowerCardID("2")); 

	}
	
	/**
     *  Hàm này để test chức năng lấy số lượng sách mà người dùng vẫn đang giữ chưa trả.
     * @throws SQLException
     * @throws ClassNotFoundException
     * @see ClassNotFoundException
     * @see SQLException
     * @see ParserConfigurationException
     */
	@Test
	public void testGetNumberUnpaidBook() throws ClassNotFoundException, SQLException {
        assertEquals(1, a.getNumberUnpaidBook("1")); 
        assertEquals(0, a.getNumberUnpaidBook("2")); 
	}
	
	/**
     *  Hàm này để test chức năng lấy ra ngày hết hạn của thẻ mượn.
     * @throws SQLException
     * @throws ClassNotFoundException
     * @see ClassNotFoundException
     * @see SQLException
     * @see ParserConfigurationException
     */
	@Test
	public void testGetExprireDayBorrowCard() throws ClassNotFoundException, SQLException {
		a.getNumberUnpaidBook("1");
		assertEquals("08-09-2017", a.getExprireDayBorrowCard("1")); 
		a.getNumberUnpaidBook("2");
		assertEquals("09-09-2017", a.getExprireDayBorrowCard("2")); 
	}
	
	/**
     *  Hàm này để test chức năng chấp thuận cho mượn cùng các thông tin đi kèm.
     * @throws SQLException
     * @throws ClassNotFoundException
     * @see ClassNotFoundException
     * @see SQLException
     * @see ParserConfigurationException
     */
	@Test
	public void testAcceptRequest() throws ClassNotFoundException, SQLException {
		a.getNumberUnpaidBook("1");
		String id = "1";
		String date = "22-02-2017";
		String note = "Thieu tien coc.";
		a.acceptRequest(id, date, note);
		connectDB();
		PreparedStatement ps = conn.prepareStatement(GET_INFO_REQEST);
		ps.setString(1, id);
		ps.executeQuery();
		ResultSet rs = ps.executeQuery();
		assertEquals(Constants.LENT, rs.getInt("State")); 
		assertEquals(note, rs.getString("Note")); 
		assertEquals(date, rs.getString("IntendedReturnDate")); 

		closeDB();		
		connectDB();
		ps = conn.prepareStatement(UPDATE_STATE);
		ps.setString(1, "3");
		ps.setString(2, id);
		ps.executeUpdate();
		ps = conn.prepareStatement(this.UPDATE_INTENDEDRETURN);
		ps.setString(1, "");
		ps.setString(2, id);
		ps.executeUpdate();
		ps = conn.prepareStatement(this.UPDATE_NOTE);
		ps.setString(1, "");
		ps.setString(2, id);
		ps.executeUpdate();
		closeDB();
	}
	
	/**
     *  Hàm này để test chức năng không chấp thuận cho mượn.
     * @throws SQLException
     * @throws ClassNotFoundException
     * @see ClassNotFoundException
     * @see SQLException
     * @see ParserConfigurationException
     */
	@Test
	public void testDenyRequest() throws ClassNotFoundException, SQLException {
		a.getNumberUnpaidBook("1");
		String id = "1";
		a.denyRequest(id);
		connectDB();
		PreparedStatement ps = conn.prepareStatement(GET_INFO_REQEST);
		ps.setString(1, id);
		ps.executeQuery();
		ResultSet rs = ps.executeQuery();
		assertEquals(Constants.AVAIABLE, rs.getInt("State")); 
		assertEquals("", rs.getString("Note")); 
		assertEquals("", rs.getString("IntendedReturnDate")); 

		closeDB();		
		connectDB();
		ps = conn.prepareStatement(UPDATE_STATE);
		ps.setString(1, "3");
		ps.setString(2, id);
		ps.executeUpdate();
		ps = conn.prepareStatement(this.UPDATE_INTENDEDRETURN);
		ps.setString(1, "");
		ps.setString(2, id);
		ps.executeUpdate();
		ps = conn.prepareStatement(this.UPDATE_NOTE);
		ps.setString(1, "");
		ps.setString(2, id);
		ps.executeUpdate();
		closeDB();
	}
}
