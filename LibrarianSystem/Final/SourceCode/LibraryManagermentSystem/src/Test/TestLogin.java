/*
 * Bản quyền thuộc về Lê Đình Phúc - MSSV: 20132979
 */
package Test;

import static org.junit.Assert.*;

import java.sql.SQLException;

import javax.xml.parsers.ParserConfigurationException;

import org.junit.BeforeClass;
import org.junit.Test;

import common.Constants;
import function.controller.*;
import function.entity.*;
/**
 * Class này để kiểm tra các hàm trong class AcceptBorrowBookController.
 * @author Lê Đình Phúc
 */
public class TestLogin {
	private LoginController a = new LoginController();
	/**
     *  Hàm này để test chức năng đăng nhập.
     * @throws SQLException
     * @throws ClassNotFoundException
     * @see ClassNotFoundException
     * @see SQLException
     * @see ParserConfigurationException
     */
	@Test
	public void test() throws ClassNotFoundException, SQLException {
        assertEquals(Constants.LOGIN_WRONG_EMAIL, a.checkLogin("aaaaaaaaaaaa@gmail.com", "abcd")); 
        assertEquals(Constants.LOGIN_WRONG_PASSWORD, a.checkLogin("lephuc", "123")); 
        assertEquals(Constants.LOGIN_SUCCESS, a.checkLogin("lephuc", "123456")); 
        assertEquals(Constants.LOGIN_SUCCESS, a.checkLogin("hoatran", "123456")); 
        assertEquals(Constants.LOGIN_SUCCESS, a.checkLogin("admin", "123456")); 
	}

}
