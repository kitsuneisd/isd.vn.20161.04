package common;

public class Constants {
	public static final int LOGIN_BLOCKED = 0;
	public static final int LOGIN_CHANGE_PASSWORD = 1;
	public static final int LOGIN_SUCCESS = 2;
	public static final int LOGIN_WRONG_PASSWORD = 3;
	public static final int LOGIN_WRONG_EMAIL = 4;
	
	public static final int NOT_ACTIVATE = 0;
	public static final int BORROWER = 1;
	public static final int LIBRARIAN = 2;
	public static final int ADMIN = 3;
	
	public static final int AVAIABLE = 1;
	public static final int REFERENCED = 2;
	public static final int BORROW = 3;
	public static final int LENT = 4;

}
