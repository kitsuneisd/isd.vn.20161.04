/**
 * @author Nguyen Phuc Long
 * Ask user to enter an integer from keyboard (use Integer class)
 * Display if the number is a prime,
 * a square number or/and a perfect number
 */
package hust.group04.hw01;

public class FirstNumberApp {
	private hust.group04.hw01.Integer i = new hust.group04.hw01.Integer();
	
	public void enterAnInteger() {
		System.out.print("Enter an integer from keyboard: ");
		
		i.getItegerFromKeyboard();		
		
		if (i.isPrimeNumber()) {
			System.out.println("Integer " + i.getValue() + " is a prime number");
		}
		
		if (i.isSquareNumber()) {
			System.out.println("Integer " + i.getValue() + " is a square number");
		}
		
		if (i.isPerfectNumber()) {
			System.out.println("Integer " + i.getValue() + " is a perfect number");
		}
	}
}
