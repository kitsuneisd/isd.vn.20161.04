package hust.group04.hw01;
//import java.util.Date;
import java.util.Calendar;
import java.util.Scanner;
public class HelloWorld {
	private static Scanner scanIn;

	public static void main (String args[]){
		scanIn = new Scanner(System.in);
		String name = "";
		int YearOfBirth = 0;
		try{
			System.out.println("what is your name?");
			name = scanIn.nextLine();
			System.out.println("what is your year of birth?");
			YearOfBirth = scanIn.nextInt();
		}catch (Exception e){
			System.out.println("ERROR!!!");
		}
		Calendar cal = Calendar.getInstance();
		int currentYear = cal.get(Calendar.YEAR);
		int age = currentYear - YearOfBirth;
		System.out.println("hello "+name+". You are "+age+" years old");
	}

}
