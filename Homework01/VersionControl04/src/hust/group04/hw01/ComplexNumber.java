package hust.group04.hw01;

public class ComplexNumber {
	 private final double re;   // the real part
	 private final double im;   // the imaginary part

	 // create a new object with the given real and imaginary parts
	 public ComplexNumber(double real, double imag) {
		 re = real;
		 im = imag;
	 }
	    
	 
	 public static void main(String[] args) {
		 ComplexNumber a = new ComplexNumber(20,30);
		 ComplexNumber b = new ComplexNumber(30,20);
		 ComplexNumber c = new ComplexNumber(30,0);
		 ComplexNumber d = new ComplexNumber(0,20);


		 System.out.println("a = " + a.toString());
		 System.out.println("b = " + b.toString());
		 System.out.println("c = " + c.toString());
		 System.out.println("d = " + d.toString());
		 
		 System.out.println("a + b = " + a.plus(b).toString());
		 System.out.println("a - b = " + a.minus(b).toString());
		 System.out.println("a * b = " + a.times(b).toString());
		 System.out.println("a * c = " + a.times(c).toString());
		 System.out.println("a * 12 = " + a.scale(12).toString());
		 System.out.println("a : b = " + a.divides(b).toString());
		 System.out.println("a : c = " + a.divides(c).toString());
		 System.out.println("a : d = " + a.divides(d).toString());
		 System.out.println("b : b = " + b.divides(b).toString());
		 System.out.println("c : b = " + c.divides(b).toString());
		 System.out.println("c : d = " + c.divides(d).toString());
		 System.out.println("d : c = " + d.divides(c).toString());
		 System.out.println("d : d = " + d.divides(d).toString());
	 }
	 
	 
	 // return a string representation of the invoking Complex object
	 public String toString() {
		 if (im == 0) return re + "";
	     if (re == 0) return im + "i";
	     if (im <  0) return re + " - " + (-im) + "i";
	     return re + " + " + im + "i";
	 }
	 
	 // return a new Complex object whose value is (this + b)
	 public ComplexNumber plus(ComplexNumber b) {
		 ComplexNumber a = this;             // invoking object
	     double real = a.re + b.re;
	     double imag = a.im + b.im;
	     return new ComplexNumber(real, imag);
	 }
	 
	 // return a new Complex object whose value is (this - b)
	 public ComplexNumber minus(ComplexNumber b) {
		 ComplexNumber a = this;
	     double real = a.re - b.re;
	     double imag = a.im - b.im;
	     return new ComplexNumber(real, imag);
	 }
	 
	 // return a new Complex object whose value is (this * b)
	 public ComplexNumber times(ComplexNumber b) {
		 ComplexNumber a = this;
	     double real = a.re * b.re - a.im * b.im;
	     double imag = a.re * b.im + a.im * b.re;
	     return new ComplexNumber(real, imag);
	 }
	    
	 // return a / b
	 public ComplexNumber divides(ComplexNumber b) {
	  	ComplexNumber a = this;
	  	return a.times(b.reciprocal());
	 }
	    
	 // return a new Complex object whose value is the reciprocal of this
	 public ComplexNumber reciprocal() {
		double scale = re*re + im*im;
	    return new ComplexNumber(re / scale, -im / scale);	
	 }
	 // return a new object whose value is (this * alpha)
	 public ComplexNumber scale(double alpha) {
	    return new ComplexNumber(alpha * re, alpha * im);
	 }
	 
}
