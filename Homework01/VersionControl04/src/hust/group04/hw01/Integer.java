/**
 * @author Nguyen Phuc Long
 */
package hust.group04.hw01;

import java.util.ArrayList;
import java.util.Scanner;

public class Integer {
	private int i;
	
	/**
	 * input an integer number from keyboard
	 */
	public void getItegerFromKeyboard() {
		Scanner input = new Scanner(System.in);
		int tmp = input.nextInt();
		setI(tmp);
		input.close();
	}

	/**
	 * @return the i
	 */
	public int getValue() {
		return i;
	}



	/**
	 * @param i the i to set
	 */
	private void setI(int i) {
		this.i = i;
	}


	/**
	 * check if the integer is a prime number
	 * @return true if integer is a prime number,
	 * false if integer is a composite number
	 */
	public boolean isPrimeNumber() {
		int t = getValue();
		if (t < 2) {
			return false;
		}
		
		int j;
		for (j = 2; j <= Math.sqrt(t); j++) {
			if (t % j == 0) {
				return false;
			}
		}
		return true;
	}
	
	/**
	 * check if integer is a square number
	 * (i.e. the number is a square of another number)
	 * @return true if integer is a square number
	 */
	public boolean isSquareNumber() {
		int t = getValue();
		if (t < 0) {
			return false;
		}
		
		double j = Math.sqrt(t);
		if (j - (int)j > 0) {
			return false;
		}
		return true;
	}
	
	/**
	 * check if integer is a perfect number
	 * (i.e. a positive integer that is equal to the sum
	 * of its proper positive divisors, that is, the sum
	 * of its positive divisors excluding the number itself)
	 * @return true if integer is a perfect number
	 */
	public boolean isPerfectNumber() {
		if (getValue() <= 0) {
			return false;
		}
		
		ArrayList<java.lang.Integer> divisors = getPositiveDivisors();
		
		int t = 2 * getValue();
		
		for (int j : divisors) {
			t -= j;
			if (t < 0) {
				return false;
			}
		}
		
		if (t == 0) {
			return true;
		}
		
		return false;
	}
	
	/**
	 * return ArrayList integer which store divisors of a integer
	 */
	private ArrayList<java.lang.Integer> getPositiveDivisors() {
		int j, t = getValue();
		ArrayList<java.lang.Integer> list = new ArrayList<>();
		if (t < 0) {
			t = -t;
		}
		for (j = 1; j <= Math.sqrt(t); j++) {
			if (t % j == 0) {
				list.add(java.lang.Integer.valueOf(j));
				list.add(java.lang.Integer.valueOf(t/j));
			}
		}
		return list;
	}
}
