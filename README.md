#**I. Info**
------------------------
##Course
* Name: ITSS software development
* Term: 20161
* Lecturer: Nguyen Thi Thu Trang
##Group
* ID: 04
* Name: ISD.VN.20161.04
## Members
|Name|Student ID|Email|
|----|----------|-----|
|Nguyen Phuc Long|20132385|longnp95@gmail.com|
|Tran Thi Nhu Hoa|20131520|hoatn.hedspi@gmail.com|
|Le Dinh Phuc|20132979 |phuc.ld95@gmail.com |

#**II. Homework**
-----------------------
##1. Hw01
###1.1. Nguyen Phuc Long:
- I write 2 classes Integer and FirstNumberApp.
- What is a branch? What is a fork?: http://goo.gl/myyQFz
- Practice's screenshot: https://goo.gl/photos/SxWqR3J6KTsXqDM37

###1.2. Le Dinh Phuc
- Code class ComplexNumber with 7 operations, and 1 mothod main() for testing.
	- **return a string representation of the invoking Complex object**
	- public String toString();
	- **return a new Complex object whose value is (this + b)**
	- public ComplexNumber plus(ComplexNumber b);
	- **return a new Complex object whose value is (this - b)**
	- public ComplexNumber minus(ComplexNumber b);
	- **return a new Complex object whose value is (this * b)**
	- public ComplexNumber times(ComplexNumber b);
	- **return a new Complex object whose value is (this / b)**
	- public ComplexNumber divides(ComplexNumber b);
	- **return a new Complex object whose value is the reciprocal of this**
	- public ComplexNumber reciprocal();
	- **return a new Complex object whose value is (this * alpha)**
	- public ComplexNumber scale(double alpha);
	 
	- **In main method, i gave a few common test cases and display the results to the console screen.**

###1.3. Tran Thi Nhu Hoa
- write class HelloWorld
- some basic Git commands: https://services.github.com/kit/downloads/github-git-cheat-sheet.pdf

-----------------------------
##2. Hw02
###2.1. Glossary
Online version: http://goo.gl/y8RyVA

- Nguyen Phuc Long: 2.1 - 2.3
      - 2.1 Close course registration
      - 2.2 Course
      - 2.3 Course manager
- Le Dinh Phuc: 2.4 - 2.6
      - 2.4 Lecturer
      - 2.5 Open course infomation
      - 2.6 Semester
- Tran Thi Nhu Hoa: 2.7 - 2.11
      - 2.7 Student
      - 2.8 Register for the course
      - 2.9 Basic Information about each course
      - 2.10 CourseID
      - 2.11 Prerequisites

###2.2. Supplementary Specification
- Online version: http://goo.gl/bDpPYw
- Offline version: src/hust/group04/hw02/SupplementarySpecification.docx

###2.3. Use-case Specifications
- Nguyen Phuc Long: online version: http://goo.gl/FaPRkW
- Tran Thi Nhu Hoa: online version: https://docs.google.com/document/d/1uzp5J1ZYI2oT4pJGvS1KlpNCCnuIBlraU_NIJx6Casc/edit?usp=sharing
- Le Dinh Phuc: offline version: src/hust/group04/hw02/Lê Đình Phúc/Use-Case Specification add new course.docx

###2.4. Use-Case Diagram
- Offline version: src/hust/group04/hw02/HW2-UCDiagram.jpg